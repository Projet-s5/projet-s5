#include <stdio.h>
#include <stdlib.h>
#include "tableau.h"

#define NIVEAU_NORMAL 1
#define NIVEAU_SUSPECT 2
#define NIVEAU_CRITIQUE 3


int main()
{
    printf("test1\n");
    float seuilSuspect = 0.75;
    float seuilCritique = 0.5;


    // faire la corrélation ici

    int longeurCAmpRef = sizeof(tableauVibrationReference)/sizeof(float);
    int longeurCAmpBruit1 = sizeof(tableauVibration1)/sizeof(float);
    int longeurCAmpBruit2 = sizeof(tableauVibration2)/sizeof(float);
    int longeurCAmpBruit3 = sizeof(tableauVibration3)/sizeof(float);

    int correlRef[2500];
    int lenthRef;
    int correl1[2500];
    int lenth1;
    int correl2[2500];
    int lenth2;
    int correl3[2500];
    int lenth3;

    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibrationReference, longeurCAmpRef, correlRef, &lenthRef);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration1, longeurCAmpBruit1, correl1, &lenth1);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration2, longeurCAmpBruit2, correl2, &lenth2);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration3, longeurCAmpBruit3, correl3, &lenth3);

    int maximumRef = maximum(correlRef,lenthRef);
    int maximum1 = maximum(correl1,lenth1);
    int maximum2 = maximum(correl2,lenth2);
    int maximum3 = maximum(correl3,lenth3);

    int refSuspect = maximumRef*seuilSuspect;
    int refCritique = maximumRef*seuilCritique;

    int seuil1;
    int seuil2;
    int seuil3;

    if(maximum1>refCritique)
    {
        if(maximum1>refSuspect)
        {
            seuil1 = NIVEAU_NORMAL;
        }
        else
        {
            seuil1 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil1 = NIVEAU_CRITIQUE;
    }


    if(maximum2>refCritique)
    {
        if(maximum2>refSuspect)
        {
            seuil2 = NIVEAU_NORMAL;
        }
        else
        {
            seuil2 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil2 = NIVEAU_CRITIQUE;
    }



    if(maximum3>refCritique)
    {
        if(maximum3>refSuspect)
        {
            seuil3 = NIVEAU_NORMAL;
        }
        else
        {
            seuil3 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil3 = NIVEAU_CRITIQUE;
    }

    printf("test5\n");
    printf("seuil 1 : %d\nseuil2 : %d\nseuil 3 : %d\n", seuil1,seuil2,seuil3);



    return 0;
}



