/*
 * main.c
 */

#include <stdio.h>
#include <dsk6713.h>
#include <csl_mcbsphal.h>
#include "DSK6713_AIC23.h" //codec support
#include "Fonction_projet.h" //codec support
#include "Interpretation.h"
#include "Interface.h"


float signal[TAILLE_SON];
float valeurConversion[TAILLE_SON];
int messageRecu = 0;
int valeurMessage = 0;
int EtablirValeurReference = 0;
int vibrationComplete;

//test
int TabDonnees[399] = {0};
#pragma DATA_ALIGN(TabDonnees, 512);


int main(void) {

    DSK6713_init(); // Initialize the board support library

    //Parametres pour DEMO
    LED0(); // En attente (demo)
    messageRecu = true;
    valeurMessage = DEMARRER;
    vibrationComplete = true;

    //int sig1[] = {1,2,3,2,1};
    //int sig2[] = {9,8,7,6,5};

    unsigned int sizeRes = 0;
    ASMCorrCall(tableauVibrationReference,2000,tableauVibrationReference,2000,TabDonnees,&sizeRes);
    outputCorrelation(TabDonnees, sizeRes,1);

    ASMCorrCall(tableauVibrationReference,2000,tableauVibration1,2000,TabDonnees,&sizeRes);
    outputCorrelation(TabDonnees, sizeRes,2);
    free(tableauVibration1);

    ASMCorrCall(tableauVibrationReference,2000,tableauVibration2,2000,TabDonnees,&sizeRes);
    outputCorrelation(TabDonnees, sizeRes,3);
    free(tableauVibration2);

    ASMCorrCall(tableauVibrationReference,2000,tableauVibration3,2000,TabDonnees,&sizeRes);
    outputCorrelation(TabDonnees, sizeRes,4);
    free(tableauVibration3);
    free(tableauVibrationReference);



    //ASMCorrCall(sig1,5,sig2,5,TabDonnees,&sizeRes);//corrRes,&sizeRes);
    //ASMCorrCall(sig1,5,sig2,5,TabDonnees,&sizeRes);//corrRes,&sizeRes);


    //free(sig2);
    //free(corrRes);
    /*
    while(1)
    {
        if (messageRecu == true)
        {
           traduireMessage(valeurMessage);
           messageRecu = false;
        }

        //if (moteurMarche == true && DSK6713_rget(DSK6713_USER_REG) & 0x10) //verifie que la switch0 est activer (peut importe letat des leds ou des autres switch)
        //{
        //    EtablirValeurReference = true;
        //}

        if(messageRecu == false && moteurMarche == 1) //redefenir le true de la valeur ref pour projet mais ok pour demo
        {
            if(vibrationComplete == true)
            {

                int seuil;

                seuil = Interpretation(1);

                switch(seuil)
                {
                    case NIVEAU_NORMAL:
                        LED0(); //detecte niveau de vibration normal
                        break;
                    case NIVEAU_SUSPECT:
                        LED1(); //detecte niveau de vibration suspect
                        break;
                    case NIVEAU_CRITIQUE:
                        LED2(); //detecte niveau de vibration critique
                        break;
                }


                seuil = Interpretation(2);
                switch(seuil)
                {
                    case NIVEAU_NORMAL:
                        LED0(); //detecte niveau de vibration normal
                        break;
                    case NIVEAU_SUSPECT:
                        LED1(); //detecte niveau de vibration suspect
                        break;
                    case NIVEAU_CRITIQUE:
                        LED2(); //detecte niveau de vibration critique
                        break;
                }



                seuil = Interpretation(3);
                switch(seuil)
                {
                    case NIVEAU_NORMAL:
                        LED0(); //detecte niveau de vibration normal
                        break;
                    case NIVEAU_SUSPECT:
                        LED1(); //detecte niveau de vibration suspect
                        break;
                    case NIVEAU_CRITIQUE:
                        LED2(); //detecte niveau de vibration critique
                        break;
                }


            }
        }
    }*/
	return 0;
}
