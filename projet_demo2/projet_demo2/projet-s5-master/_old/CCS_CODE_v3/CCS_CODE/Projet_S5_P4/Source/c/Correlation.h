/*
 * Tableau_Vibration.h
 *
 *  Created on: 27 f�vr. 2019
 *      Author: melo2301
 */

#ifndef SOURCE_C_CORRELATION_H_
#define SOURCE_C_CORRELATION_H_


#include <stdio.h>
#include <stdlib.h>
#include "Fonction_projet.h"
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support




void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);

void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes);

void ASMCorr(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes);

#endif /* SOURCE_C_CORRELATION_H_*/
