/* Fonction.h */

#ifndef SOURCE_C_FONCTION_PROJET_H_
#define SOURCE_C_FONCTION_PROJET_H_



//constante

#define ARRETER 1
#define DEMARRER 2
#define FAUX 0
#define NIVEAU_CRITIQUE 3
#define NIVEAU_NORMAL 1
#define NIVEAU_SUSPECT 2
#define RESET_TEMOIN 3
#define TAILLE_SON 2500 // a determiner
#define TAILLE_TEMPERATURE 0 // a determiner
#define TAILLE_VIBRATION 0 // a determiner
#define VRAI 1
#define SEUIL_SUSPECT 0.75
#define SEUIL_CRITIQUE 0.5


//Adresse des registres importants

#define McBSP0_R 0x018C0000 //receive
#define McBSP1_R 0x01900000 //receive

#define McBSP0_T 0x018C0004
#define McBSP1_T 0x01900004

//variable globale
extern int moteurMarche;
extern int true;
extern int false;


//Fonction
void enableInterrupts(void);
void configAndStartTimer1(void);
void EteindreTemoinLumineux();
void LED0(void);
void LED1(void);
void LED2(void);
void LED3(void);
void traduireMessage(int valeurMessage);
void DemarrerMoteur();
void ArreterMoteur();
void patcher_tableau();


//correlation
void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);
void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes);
void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);
void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes);


#endif /* SOURCE_C_FONCTION_PROJET_H_ */


