/*
 * Tableau_Vibration.c
 *
 *  Created on: 27 f�vr. 2019
 *      Author: melo2301
 */

#include"Interpretation.h"


int Maximum(int *tableauTest, int longeur)
{

    int i;
    int valeurMax = 0;
    for(i=0;i<longeur;i++)
    {
        if(tableauTest[i] > valeurMax)
        {
            valeurMax = tableauTest[i];
        }
    }
    return valeurMax;
}


int Interpretation(int numTest) // 1, 2 ou 3
{

    unsigned int longeurCAmpRef = sizeof(tableauVibrationReference)/sizeof(float);

    int correlRef[2500];
    unsigned int lenthRef;

    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibrationReference, longeurCAmpRef, correlRef, &lenthRef);
    int maximumRef = Maximum(correlRef,lenthRef);

    int refSuspect = maximumRef*SEUIL_SUSPECT;
    int refCritique = maximumRef*SEUIL_CRITIQUE;

    int correlVibration[2500];
    unsigned int lenthV;

    if(numTest == 1)
    {
        unsigned int longeurCAmpBruit1 = sizeof(tableauVibration1)/sizeof(float);

        ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration1, longeurCAmpBruit1, correlVibration, &lenthV);
    }

    else if(numTest == 2)
    {
        unsigned int longeurCAmpBruit2 = sizeof(tableauVibration2)/sizeof(float);

        ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration2, longeurCAmpBruit2, correlVibration, &lenthV);
    }

    else if(numTest == 3)
    {
        unsigned int longeurCAmpBruit3 = sizeof(tableauVibration3)/sizeof(float);

        ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration3, longeurCAmpBruit3, correlVibration, &lenthV);
    }

    else
    {
        return 0;
    }


    int maximumV = Maximum(correlVibration,lenthV);
    int seuil;

    if(maximumV>refCritique)
    {
        if(maximumV>refSuspect)
        {
            seuil = NIVEAU_NORMAL;
        }
        else
        {
            seuil = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil = NIVEAU_CRITIQUE;
    }



    // faire la corr�lation ici

    return seuil;
}
