/*
 * Fonction
 */

//Headers
#include <stdio.h>
#include "Fonction_projet.h"
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support
//

//variable globale
int moteurMarche = 0;
int true = 1;
int false = 0;
//

//definition de fonction

void enableInterrupts(void); //fonction asm

void configAndStartTimer1(void){
    * (unsigned int*) TIMER_1_CTL &= 0xFFFFFF7F; ///HLD = 0
    * (unsigned int*) TIMER_1_PRD = 3515; // Met a 8 KHz
    * (unsigned int*) TIMER_1_CTL  = 0x301;
    * (unsigned int*) TIMER_1_CTL |= 0XC0; // HDL et GO to 1
}



void LED0(void){
    DSK6713_rset(DSK6713_USER_REG, 0x1); //allume leds 0
}

void LED1(void){
    DSK6713_rset(DSK6713_USER_REG, 0x2); //allume leds 1
}

void LED2(void){
    DSK6713_rset(DSK6713_USER_REG, 0x4); //allume leds 2
}

void LED3(void){
    DSK6713_rset(DSK6713_USER_REG, 0x8); //allume leds 3
}

void EteindreTemoinLumineux()
{
    DSK6713_rset(DSK6713_USER_REG, 0x0); //eteint les leds
}

void traduireMessage(int valeurMessage)
{
    if (valeurMessage == DEMARRER)
    {
        DemarrerMoteur();
    }

    if (valeurMessage == RESET_TEMOIN)
     {
         EteindreTemoinLumineux();
     }

    if (valeurMessage == ARRETER)
     {
         ArreterMoteur();
     }
}

void DemarrerMoteur()
{
    //implementer le signal permettant le demarage du moteur
    moteurMarche = true;
}

void ArreterMoteur()
{
    //implementer le signal permettant l arret du moteur
}

//


