	.def _enableInterrupts

	.data

mask_GIE .set 0x01
mask_NMIE .set 0x8812

_enableInterrupts
	.asmfunc

	MVC CSR,B0
	OR B0, mask_GIE, B0
	MVC B0, CSR

	MVC IER, B0
	MVKL mask_NMIE, B1
	MVKH mask_NMIE, B1
	OR B0, B1, B0
	MVC B0, IER

	B B3
	NOP 5

	.endasmfunc
