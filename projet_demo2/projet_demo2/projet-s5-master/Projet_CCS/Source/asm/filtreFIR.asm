;****************************************************************************************
; APPEL :
; short * direct1FIR_ASM(short *cPtr, short ech, const short h[], short *y);
;
; A4 : Adresse du pointeur sur l'�chantillon pr�c�dent
; B4 : Valeur de l'�chantillon courant
; A6 : Adresse du d�but du tableau de coefficients du filtre, h[]
; B6 : Adresse de l'�chantillon de sortie
;
; Return : la nouvelle adresse de cPtr (A4)
;
; NB:
; 1) La longueur du filtre passe-haut, N = 31 (ordre = 30), est fixe (hardcoded)
;    et n'est donc pas pass�e comme param�tre � la fonction
; 2) Comme le mode d'adressage circulaire est employ� pour le tampon, il n'est pas n�cessaire
;    de sp�cifier l'adresse de d�but du tampon mais simplement l'adresse de l'�chantillon
;    courant. En effet, quand le pointeur du tampon est incr�ment� ou d�cr�ment�, le
;    mode d'adressage circulaire force le pointeur � rester dans le tampon ("wrapping"). Il suffit simplement
;    de sp�cifier la bonne longueur avec l'AMR et de bien aligner le tampon lors de sa
;    d�claration dans le programme C avec "#pragma DATA_ALIGN"
;
;****************************************************************************************

		.def	_filtreFIR
_filtreFIR:

; M�moriser le contenu de AMR
			MVC	AMR, B5
			STW	B5,*--B15
;			STW A10, *--B15
;			STW A11, *--B15
;			STW A12, *--B15
;			STW A13, *--B15
;			STW A14, *--B15
;
;			STW B10, *--B15
;			STW B11, *--B15
;			STW B12, *--B15
;			STW B13, *--B15

; Ajuster le registre AMR
			MVKL 0x00A08000, B9	; Utiliser B7 comme pointeur avec BK1 comme compteur vers
			MVKH 0x00A08000, B9	; Le tampon contient 128 �chantillons de 16 bits chacuns, donc 256 octets (2 X 128)
			MVC B9, AMR

; Charger l'adresse de l'�chantillon pr�c�dent du tampon dans B7 :
			MV A4, B7

; �crire l'�chantillon courant dans le tampon pTampon[+1] = x
			STH B4, *++B7

; Retourner l'adresse de l'�chantillon courant : pTampon = x
			MV B7, A4

; Longueur du filtre, N (ordre + 1, valeur impaire puisque c'est un passe-haut symm�trique)
			;MVK 127, A1
; Constante n pour la boucle FIR (n = 15 = ((N-1)/2) o� N = 31)
			MVK 31, B0

; Initialisation y[n]=0
			ZERO A11;

;**********************************************************************
; Filtrage : boucle y[n] = y[n] + x[n-k]*h[k], pour k = 0..N, o� N = 63
;            et o� A11 est un accumulateur auquel on sont ajout�s les
;            produits x[n-k]*h[k] � chaque it�ration, k, de la boucle.
;            Le filtre n'est pas r�cursif...
;
;            ATTENTION: les x[n] sont contenus dans le tampon circulaire
;                       et sont donc load�s avec B7 et non A4.
;**********************************************************************
;... Calcul en langage C :
;... int n = 15;
;...
;... for(n = 15; n>0; n--)
;...  y += c[15-n]*(x[15-n]+x[15+n])
;...

; Version optimiser
			MVK 31, A2
boucleFIR:
			SUB A2, B0, A1 ; operation 64-n
			MV A1, B1 ; B1 pour le LDH puisque load du registre B
			LDH.D1 *+A6[A1], A7 ; c[63-n]
		 || LDH.D2 *-B7[B1], B8 ; x[63-n]
			ADD B0, A2, B2 ; operation 63+n
			NOP 4
			LDH *-B7[B2], B9 ; x[63+n]
			NOP 5
			ADD B8, B9, B8 ; x[63-n]+x[63+n]
			MPY A7, B8, A1 ; c[63-n]*(x[63-n]+x[63+n])
			NOP 1
			ADD A11, A1, A11 ;y += c[63-n]*(x[63-n]+x[63+n])
			SUB B0, 1, B0 ; Pour le nombre de boucle a faire
			[B0] B boucleFIR
			NOP 5

			;Pour le coeficient central
			MV A2, B1
			LDH.D1 *+A6[A2], A7 ; c[63]
		 || LDH.D2 *-B7[B1], B8 ; x[63]
			NOP 5
			MPY A7, B8, A1 ; c[63-n]*(x[63-n]+x[63+n])
			NOP 1
			ADD A11, A1, A11 ;y += c[63-n]*(x[63-n]+x[63+n])

			SHR A11, 15, A11
;...
;...
;...

; Enregistrer la sortie y[n]
			STH A11, *B6

; DEBUGGAGE: balancer l'�chantillion d'entr�e directement dans la sortie
			;STH B4, *B6

; R�tablir le contenu de AMR

;			LDW *B15++, B13
;			NOP 4
;			LDW *B15++, B12
;			NOP 4
;			LDW *B15++, B11
;			NOP 4
;			LDW *B15++, B10
;			NOP 4
;
;			LDW *B15++, A14
;			NOP 4
;			LDW *B15++, A13
;			NOP 4
;			LDW *B15++, A12
;			NOP 4
;			LDW *B15++, A11
;			NOP 4
;			LDW *B15++, A10
;			NOP 4

			LDW	*B15++,B5
			NOP	4
			MVC	B5,AMR

; Retourner l'adresse � la fonction qui a fait l'appel (return)
			B B3
			NOP 5

		.end
