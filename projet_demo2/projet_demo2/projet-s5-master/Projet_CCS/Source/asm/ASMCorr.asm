	.def _ASMCorr


_ASMCorr
	.asmfunc
	;;Valeurs recues sur A4,B4,A6,B6,A8,B8
	; A4 = sig1 , B4 = size sig1
	; A6 = sig2 , B6 = size sig2
	; A8 = sigRes, B8 = size sigRes

	;; Protection de l'AMR et du CSR sur la pile
	MVC	AMR, B5
	STW B5, *B15--[2]
	MVC CSR, B5
	STW B5, *B15--[2]

	;; Protection du contexte des variables utilisees
	STW A1, *B15--[2]
	STW A2, *B15--[2]
	STW A3, *B15--[2]
	STW A5, *B15--[2]
	STW A7, *B15--[2]
	STW A9, *B15--[2]
	STW B5, *B15--[2]
	STW B7, *B15--[2]
	STW B9, *B15--[2]

	MVK 0,A1 ;n = 0
	ADD B4,B4,B5 ; B5 = 2*N
	SUB B5,1,B5 ; B5 = 2*N-1
HigherLoop:
	MVK 0,A2 ;i = 0
	SUB B4,1,B7
	CMPGT A1,B7,B1 ;N>n
	[B1] B LowerSecondLoop
	NOP 5



LowerFirstLoop:
		CMPGT A2,A1,A3 ;if i>n go back to top
		MV A3,B1
		[B1] B AfterLoop
		NOP 5

		;First index Nres-n
		;ADD A1,1,A5   ;**** n+1
		SUB B5,A1,A5 ;
		SUB A5,1,A5

		;Second index end-n+i index sig1
		SUB B4,A1,A7 ; end-n
		ADD A7,A2,A7 ;(end-n)+i
		SUB A7,1,A7


		LDW *A4[A7],B7 ;Signal1
		NOP 4
		LDW *A6[A2],B9 ;Signal2
		NOP 4
		MPY B7,B9,B7 ;Signal1In(end-n+i)*Signal2In(i)
		NOP 1
		LDW *A8[A5],B9 ; CorrVector(Nres-n+1)
		NOP 4
		ADD B7,B9,B7 ; CorrVector(Nres-n+1) + Signal2In(i)*Signal1In(end-n+i)
		STW B7,*A8[A5]

		ADD A2,1,A2 ;i++
		B LowerFirstLoop
		NOP 5

LowerSecondLoop:

		MPY 2,B4,A7 ;2*N **********************************************
		NOP 1
		SUB A7,A1,A7 ;2*N-n
		SUB A7,1,A7
		SUB A7,1,B7
		CMPGT A2,B7,B1 ;if i>(2*N-n) go back to top **
		[B1] B AfterLoop
		NOP 5

		;First index (Nres-n)+1 **
		SUB B5,A1,A5
		SUB A5,1,A5

		;Second index 2*N-n


		;Troisieme index
		SUB B4,A7,A9 ;end-(2*N-n)
		ADD A9,A2,A9 ;end-(2*N-n)+i

		;ADD 1,A2,A2 ****


		LDW *A4[A2],B7 ;Signal1
		NOP 4
		LDW *A6[A9],B9 ;Signal2
		NOP 4
		MPY B7,B9,B7 ;Signal1In(end-n+i)*Signal2In(i)
		NOP 1
		LDW *A8[A5],B9 ; CorrVector(Nres-n+1)
		NOP 4
		ADD B7,B9,B7 ; CorrVector(Nres-n+1) + Signal2In(i)*Signal1In(end-n+i)
		STW B7,*A8[A5]


		ADD A2,1,A2 ;i++
		B LowerSecondLoop
		NOP 5

AfterLoop:
	ADD A1,1,A1 ;n++

	CMPGT B5,A1,A2 ;Nres>n
	[A2] B HigherLoop
	NOP 5
	;else n>Nres, alors finito
endOfFunction:




	; Restauration du contexte
	LDW *++B15[2],B9
	NOP 4
	LDW *++B15[2],B7
	NOP 4
	LDW *++B15[2],B5
	NOP 4
	LDW *++B15[2],A9
	NOP 4
	LDW *++B15[2],A7
	NOP 4
	LDW *++B15[2],A5
	NOP 4
	LDW *++B15[2],A3
	NOP 4
	LDW *++B15[2],A2
	NOP 4
	LDW *++B15[2],A1
	NOP 4

	;; Restauration de l'AMR et du CSR
	LDW *++B15[2],B5
	NOP 4
	MVC B5, CSR
    LDW *++B15[2], B5
    NOP 4
	MVC B5, AMR

	B B3
	NOP 5

	.endasmfunc
