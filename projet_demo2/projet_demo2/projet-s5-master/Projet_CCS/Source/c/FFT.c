/*
 * FFT.c
 *
 *  Created on: Apr 1, 2019
 *      Author: charles-frederick
 */

#define length256 256
#define length512 512
#define length1024 1024

#include "FFT.h"
#include "C:\ti\ccsv7\ccs_base\C6700\dsplib\include\DSPF_sp_cfftr2_dit.h"
#include "Twiddle.h"

#include <math.h>

int FFTProcess(int* sig, unsigned lengthSig)
{
    int returnVal = 0;
    //do the Appropriate FFT or return error.
    unsigned i = 0;
    switch(lengthSig)
    {
        case length256:
        {
            //Case of 256 ech input signal
            float fftRes[2*length256];
            for(; i < length256; i++)
            {
                fftRes[2*i] = sig[i];
                fftRes[2*i+1] = 0;
            }
            DSPF_sp_cfftr2_dit(fftRes,w256,lengthSig);
            break;
        }
        case length512:
        {
            //Case of 512 ech input signal
            float fftRes[2*length512];
            for(; i < length512; i++)
            {
                fftRes[2*i] = sig[i];
                fftRes[2*i+1] = 0;
            }
            DSPF_sp_cfftr2_dit((float*)sig,w512,lengthSig);
            break;
        }
        default:
        {
            //Case of other random amount of ech input signals
            returnVal = ERROR;
            break;
        }
    }

    //Find the average of the fft
    int averageFFTValue = FFTAverageComplex(sig,lengthSig);

    //Find the local max around the 8k freq
    unsigned normalizedStartID = floor((FREQ_CENTER-FREQ_MARGIN)/lengthSig);
    unsigned normalizedEndID = floor((FREQ_CENTER+FREQ_MARGIN)/lengthSig);
    int local8KHzMax = FFTMaxLocal(sig,lengthSig,normalizedStartID,normalizedEndID);

    //Do the fftAnalysis logic from matlab: condition for identification: if LocalMaxVal(8k) > 1.5*AvgVal
    if(local8KHzMax > (1.5*averageFFTValue))
    {
        returnVal = SIGNAL_DETECTED;
    }
    else
    {
        returnVal = NO_SIGNAL_DETECTED;
    }
    return returnVal;

}
int FFTAverage(int* sig,unsigned lengthSig)//, unsigned startIndex, unsigned endIndex)
{
    int average, i;
    for(i = 0, average = 0; i < lengthSig; i++)
    {
        average = average + sig[i];
    }
    return average/lengthSig;
}

int FFTAverageLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int average, i;
        for(i = startID, average = 0; i < endID; i++)
        {
            average = average + sig[i];
        }
        return average/lengthSig;
    }
    else
        return ERROR;
}

int FFTAverageComplex(int* sig, unsigned lengthSig)
{
    int average, i;
    for(i = 0, average = 0; i < lengthSig/2; i++)
    {
        average = average + sqrt(sig[2*i]+sig[2*i+1]);
    }
    return average/(lengthSig)*2;
}

int FFTAverageComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int average, i;
        for(i = startID, average = 0; i < endID/2; i++)
        {
            average = average + sqrt(sig[2*i]+sig[2*i+1]);
        }
        return average/(endID-startID)*2;
    }
    else
        return ERROR;
}

int FFTMax(int* sig, unsigned lengthSig)//, unsigned startIndex, unsigned endIndex)
{
    int max, i;
    for(i = 0, max = 0; i < lengthSig; i++)
    {
        if(sig[i] > max)
            max = sig[i];
    }
    return max;
}

int FFTMaxLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int max, i;
        for(i = startID, max = 0; i < endID; i++)
        {
            if(sig[i] > max)
                max = sig[i];
        }
        return max;
    }
    else
        return ERROR;
}

int FFTMaxComplex(int* sig, unsigned lengthSig)
{
    int max, i;
    for(i = 0, max = 0; i < lengthSig/2; i++)
    {
        unsigned val = sqrt(sig[2*i]*sig[2*i+1]);
        if(sig[i] > max)
            max = sig[i];
    }
    return max;
}

int FFTMaxComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int max, i;
        for(i = startID, max = 0; i < endID/2; i++)
        {
            unsigned val = sqrt(sig[2*i]*sig[2*i+1]);
            if(sig[i] > max)
                max = sig[i];
        }
        return max;
    }
    else
        return ERROR;
}
