/*
 * Trame.c
 *
 *  Created on: 4 avril 2019
 *      Author: lapj2123
 */

#include "../../includes/TableauSignaux.h"

short Tram(short tableauEntree[], short type, short curseur, int sortie[])
{
    short nbEch = 0;
    short curseurRetour = 0;

    switch (type) {
        case 1: //vibration
            nbEch = 128;
            break;

        case 2: //son
            nbEch = 256;
            break;
    }
    int i = 0;
    for(;i<nbEch; i++){
        sortie[i] = (int)tableauEntree[curseur + i];
    }
    curseurRetour = curseur + nbEch;
    return curseurRetour;
}
