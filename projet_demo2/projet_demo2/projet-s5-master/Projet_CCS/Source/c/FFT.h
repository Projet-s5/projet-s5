/*
 * FFT.h
 *
 *  Created on: Apr 1, 2019
 *      Author: charles-frederick
 */
static int ERROR = 666;
static int SIGNAL_DETECTED = 21;
static int NO_SIGNAL_DETECTED = 22;
static int FREQ_CENTER = 8000; //Hz
static int FREQ_MARGIN = 500; // Hz

int FFTProcess(int* sig, unsigned lengthSig); //returns 1 if error.
int FFTAverage(int* sig, unsigned lengthSig);
int FFTAverageLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTAverageComplex(int* sig, unsigned lengthSig);
int FFTAverageComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTMax(int* sig, unsigned lengthSig);
int FFTMaxLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTMaxComplex(int* sig, unsigned lengthSig);
int FFTMaxComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);
