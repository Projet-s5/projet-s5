/*
 * Tableau_Vibration.c
 *
 *  Created on: 27 f�vr. 2019
 *      Author: melo2301
 */
#include "Tableau_Vibration.h"
#include "Fonction_projet.h"

int Maximum(int *tableauTest, int longeur)
{

    int i;
    int valeurMax = 0;
    for(i=0;i<longeur;i++)
    {
        if(tableauTest[i] > valeurMax)
        {
            valeurMax = tableauTest[i];
        }
    }
    return valeurMax;
}

/*
int Interpretation()
{

    float seuilSuspect = 0.75;
    float seuilCritique = 0.5;


    // faire la corr�lation ici

    unsigned longeurCAmpRef = sizeof(tableauVibrationReference)/sizeof(float);
    unsigned longeurCAmpBruit1 = sizeof(tableauVibration1)/sizeof(float);
    unsigned longeurCAmpBruit2 = sizeof(tableauVibration2)/sizeof(float);
    unsigned longeurCAmpBruit3 = sizeof(tableauVibration3)/sizeof(float);

    int correlRef[2500];
    unsigned lenthRef;
    int correl1[2500];
    unsigned lenth1;
    int correl2[2500];
    unsigned lenth2;
    int correl3[2500];
    unsigned lenth3;

    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibrationReference, longeurCAmpRef, correlRef, &lenthRef);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration1, longeurCAmpBruit1, correl1, &lenth1);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration2, longeurCAmpBruit2, correl2, &lenth2);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration3, longeurCAmpBruit3, correl3, &lenth3);

    int maximumRef = Maximum(correlRef,lenthRef);
    int maximum1 = Maximum(correl1,lenth1);
    int maximum2 = Maximum(correl2,lenth2);
    int maximum3 = Maximum(correl3,lenth3);

    int refSuspect = maximumRef*seuilSuspect;
    int refCritique = maximumRef*seuilCritique;

    int seuil1;
    int seuil2;
    int seuil3;

    if(maximum1>refCritique)
    {
        if(maximum1>refSuspect)
        {
            seuil1 = NIVEAU_NORMAL;
        }
        else
        {
            seuil1 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil1 = NIVEAU_CRITIQUE;
    }


    if(maximum2>refCritique)
    {
        if(maximum2>refSuspect)
        {
            seuil2 = NIVEAU_NORMAL;
        }
        else
        {
            seuil2 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil2 = NIVEAU_CRITIQUE;
    }



    if(maximum3>refCritique)
    {
        if(maximum3>refSuspect)
        {
            seuil3 = NIVEAU_NORMAL;
        }
        else
        {
            seuil3 = NIVEAU_SUSPECT;
        }
    }
    else
    {
        seuil3 = NIVEAU_CRITIQUE;
    }




    return 0;
}*/
