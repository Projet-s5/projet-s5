/*
 * Correlation.h
 *
 *  Created on: Mar 17, 2019
 *      Author: charles-frederick
 */

#ifndef SOURCE_C_CORRELATION_H_
#define SOURCE_C_CORRELATION_H_

static int CORR_ERROR_THRESHOLD = 0; //la valeur que une erreur devrait etre detected

//void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);
//short CorrProcess(short* sig1,  unsigned length1, short* sig2, unsigned length2);
void ASMCorrCall(short* sig1, unsigned length1, short* sig2, unsigned length2, short* corrRes, unsigned* sizeRes);

void ASMCorr(int* sig1,unsigned length1,int* sig2,unsigned length2,int* corrRes);//unsigned sizeRes);
void Test();

#endif /* SOURCE_C_CORRELATION_H_ */
