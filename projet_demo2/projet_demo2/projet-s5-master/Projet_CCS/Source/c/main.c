/*
 * main.c
 */

#include <stdio.h>
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support
#include "Fonction_projet.h" //codec support

#include "Correlation.h"
#include "Interface.h"
#include "Tableau_Vibration.h"
#include "FFT.h"
#include "../../includes/TableauSignaux.h"
#include <csl_mcbsphal.h>

//Enlever le define si on ne veut pas debug l'autocorr.
#define DEBUG_CORR 0x99

//float signal[TAILLE_SON];
//float valeurConversion[TAILLE_SON];
int messageRecu = 0;
int valeurMessage = 0;
int EtablirValeurReference = 0;
int sonComplete;

unsigned mode = 0;
/*
 * mode 0 = mode test
 * mode 1 = test DSK
 */

#pragma DATA_ALIGN(trame_vibration_ref,1024);
#pragma DATA_ALIGN(trame_vibration_ref2,256);
#pragma DATA_ALIGN(corrRes_ref,64);


int i = 0;

unsigned long1 = 128;
short curseur_vibration_ref = 0;
short curseur_vibration_bruit1 = 0;
short curseur_vibration_bruit2 = 0;
int trame_vibration_ref[128];
int trame_vibration_bruit1[128];
int trame_vibration_bruit2[128];
int corrRes_ref[255];
int corrRes_bruit1[255];
int corrRes_bruit2[255];
int test1[5] = {1,2,3,4,5};


int main(void) {

    DSK6713_init(); // Initialize the board support library

   for (i=0; i<256 ; i++)
     {
       corrRes_ref[i] = 0;
     }

    curseur_vibration_ref = Tram(piezo1_baseline, 1, curseur_vibration_ref, trame_vibration_ref); //ref 1
    curseur_vibration_bruit1 = Tram(piezo_noise, 1, curseur_vibration_bruit1, trame_vibration_bruit1); //bruit 1
    curseur_vibration_bruit2 = Tram(piezo2_noise, 1, curseur_vibration_bruit2, trame_vibration_bruit2); //bruit 2

    //curseur_vibration2 = Tram(piezo2_baseline2, 1, curseur_vibration2, trame_vibration_ref2); //ref 2

    ASMCorr(trame_vibration_ref,128,trame_vibration_ref,128,corrRes_ref); //autocorr
    ASMCorr(trame_vibration_bruit1,128,trame_vibration_bruit1,128,corrRes_bruit1); //autocorr
    ASMCorr(trame_vibration_bruit2,128,trame_vibration_bruit2,128,corrRes_bruit2); //autocorr


    //ASMCorr(trame_vibration_ref2,long1,trame_vibration_ref2,long1,corrRes_ref2); //autocorr

    outputCorrelation(corrRes_ref,255, filePath1);
    outputCorrelation(corrRes_bruit1,255, filePath2);
    outputCorrelation(corrRes_bruit2,255, filePath3);

    //outputCorrelation(corrRes_ref2,256, filePath2);

   // CorrProcess(trame_vibration,  long1, trame_vibration, long1);

    /*
    if( mode == 0)
    {
        //Code de test de FFT, ****mettre sous un flag dans le real-time processing.*****
        int returnFFT = FFTProcess(tableauInput,lengthTableauInput);
        switch(returnFFT)
        {
            case ERROR:
            {
                //Happens if size is not of 256 or 512 echs.
                break;
            }
            case SIGNAL_DETECTED:
            {
                //A signal is detected at the inquired frequency defined in fft.h
                break;
            }
            case NO_SIGNAL_DETECTED:
            {
                //No signal is detected at the inquired frequency defined in fft.h
                break;
            }
        }
        int returnCorr = CorrProcess(sig1,length1,sig2,length2);
    }
    */
    /*
    else if(mode == 1)
    {
    //Parametres pour DEMO
    LED0(); // En attente (demo)
    messageRecu = true;
    valeurMessage = DEMARRER;
    sonComplete = true;

    while(1)
    {
        if (messageRecu == true)
        {
           traduireMessage(valeurMessage);
           messageRecu = false;
        }

        if (moteurMarche == true && (DSK6713_rget(DSK6713_USER_REG) & 0x10)) //verifie que la switch0 est activer (peut importe letat des leds ou des autres switch)
        {
            EtablirValeurReference = true;
        }

        if(messageRecu == false && moteurMarche == 1 && EtablirValeurReference == true) //redefenir le true de la valeur ref pour projet mais ok pour demo
        {
            if(sonComplete == true)
            {
                //fonction de correlation a implementer
                LED1(); //detecter mais pas bon signal
                LED2(); //detecter et bon signal
            }
        }
    }
    }*/
	return 0;
}
