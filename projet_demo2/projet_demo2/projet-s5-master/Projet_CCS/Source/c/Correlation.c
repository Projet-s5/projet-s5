/*
 * Correlation.c
 */

//Headers
#include <stdio.h>
#include <stdlib.h>
#include "Fonction_projet.h"
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support
#include "Correlation.h"
#include "Tableau_Vibration.h"

/*
void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes)
{
    // Pad the signals if the length is not equal
    if(length1 != length2)
    {
        if(length1 > length2)
        {
            //Pad the sig2
            length2 = length1;
        }
        else
        {
            //Pad the sig1
            length1 = length2;
        }
    }

    //Instanciate the result array
    *sizeRes = length1+length2-1;
    corrRes = (int*) calloc(*sizeRes, sizeof(unsigned));

    //Acquire the first half of data
    unsigned i = 0;
    for(; i < length1 ; i++)
    {
        unsigned j = 0;
        for(; j < i; j++)
        {
            corrRes[length1-j+1] += sig1[length1-i+j]*sig2[length2-i];
        }
    }

    //Acquire the second half of data
    i = 0;
    for(; i < length1; i++)
    {
        unsigned j = 1;
        for(; j < i; j++)
        {
            corrRes[length1+j] += sig1[length1+i-j]*sig2[length1+i];
        }
    }
    //return ResCorr;
}
*/
/*
short CorrProcess(short* sig1,  unsigned length1, short* sig2, unsigned length2)
{
    //Ready the result array.
    short* correlRes = (short*) malloc(length1);
    unsigned* lengthRes = (unsigned*) malloc(1); //bug

    ASMCorrCall(sig1,length1,sig2,length2,correlRes, lengthRes);

    //Treat the result.



#ifdef DEBUG_CORR
    //Output to file
    outputCorrelation(correlRef,lengthRef, filePath1);
#endif


    free(correlRes);
}
*/
void ASMCorrCall(short* sig1, unsigned length1, short* sig2, unsigned length2, short* corrRes, unsigned* sizeRes)
{
    // Pad the signals if the length is not equal
    if(length1 != length2)
    {
        if(length1 > length2)
        {
            //Pad the sig2
            length2 = length1;
        }
        else
        {
            //Pad the sig1
            length1 = length2;
        }
    }

    *sizeRes = length1+length2-1;
    corrRes = (short*) calloc(*sizeRes, sizeof(short));
   // ASMCorr(sig1,length1,sig2,length2,corrRes);
}
/*
void Test()
{
    //Tester la correlation
    unsigned longeurCAmpRef = sizeof(tableauVibrationReference)/sizeof(float);
    unsigned longeurCAmpBruit1 = sizeof(tableauVibration1)/sizeof(float);
    unsigned longeurCAmpBruit2 = sizeof(tableauVibration2)/sizeof(float);
    unsigned longeurCAmpBruit3 = sizeof(tableauVibration3)/sizeof(float);

    //int correlRef[2500];
    int* correlRef = (int*) malloc(2500);
    unsigned lengthRef;
    //int correl1[2500];
    int* correl1 = (int*) malloc(2500);
    unsigned length1;
    //int correl2[2500];
    int* correl2 = (int*) malloc(2500);
    unsigned length2;
    //int correl3[2500];
    int* correl3 = (int*) malloc(2500);
    unsigned length3;

    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibrationReference, longeurCAmpRef, correlRef, &lengthRef);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration1, longeurCAmpBruit1, correl1, &length1);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration2, longeurCAmpBruit2, correl2, &length2);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration3, longeurCAmpBruit3, correl3, &length3);

    //char* outputPath = "";
    outputCorrelation4(correlRef,lengthRef);//, correl1, length1, correl2, length2, correl3, length3);

    //free(folderpath);
    free(correlRef);
    free(correl1);
    free(correl2);
    free(correl3);

}
*/
