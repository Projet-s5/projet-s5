/*
 * filtreFIR.h
 *
 *  Created on: 2016-10-23
 *      Author: chap1202
 *
 */

#ifndef filtreFIR_H_
#define filtreFIR_H_

extern short * filtreFIR(short *, short, const short *, short *y);

#endif /* filtreFIR_H_ */
