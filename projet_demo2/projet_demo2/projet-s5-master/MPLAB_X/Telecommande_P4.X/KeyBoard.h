/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef KEYBOARD_H
#define	KEYBOARD_H

#include <xc.h> // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>

// TODO Insert C++ class definitions if appropriate

// TODO Insert declarations

// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>Function prototype:</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation

 //function prototypes
void LectureKeyboard(int KeyBoard[4][4]);
void AfficherTouche(int KeyBoard[4][4]);
void InitilizationKeyboard();
void ActiveColonne(int colonne);
int LireLigne(int ligne);


 // Defines
#define Y1        LATEbits.LE3        // Output Activation Colonne 1
#define Y2        LATEbits.LE2        // Output Activation Colonne 2
#define Y3        LATEbits.LE1        // Output Activation Colonne 3
#define Y4        LATEbits.LE0        // Output Activation Colonne 4
#define X1        PORTEbits.RE4        // Input Lecture Ligne 1
#define X2        PORTEbits.RE5        // Input Lecture Ligne 2
#define X3        PORTEbits.RE6        // Input Lecture Ligne 3
#define X4        PORTEbits.RE7        // Input Lecture Ligne 4

/*
 PIC connexion nom pin 

59 RE7 CLAVIER Clav_X1
60 RE6 CLAVIER Clav_X2
61 RE5 CLAVIER Clav_X3
62 RE4 CLAVIER Clav_X4
63 RE3 CLAVIER Clav_Y1
64 RE2 CLAVIER Clav_Y2
1  RE1 CLAVIER Clav_Y3 
2  RE0 CLAVIER Clav_Y4*/


#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* KEYBOARD_H */

