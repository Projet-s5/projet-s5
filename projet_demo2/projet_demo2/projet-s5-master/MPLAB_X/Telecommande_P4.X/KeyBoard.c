/*
 * File:   KeyBoard.c
 * Author: Sam
 *
 * Created on 17 mars 2019, 12:08
 */


#include "KeyBoard.h"
#include "LCD_SPI.h"


#include <stdlib.h>    // pour return (EXIT_SUCCESS)
#include <stdio.h>
#include <xc.h>        		/* XC8 General Include File */
#include <stdint.h>        	/* For uint8_t definition */
#include <stdbool.h>

void InitilizationKeyboard()
{   
    //initialisation PIN du PIC
    
    TRISE= 0b11111111; //RE0 � RE3 input(temporaire) & RE4 � RE7 input
    
    Y1 = 0;            //Output low ligne 1
    Y2 = 0;            //Output low ligne 2
    Y3 = 0;            //Output low ligne 3
    Y4 = 0;            //Output low ligne 4
}

void LectureKeyboard(int KeyBoard[4][4])  //KeyBoard[Ligne][Colonne]
{
    int colonne;
    int ligne;
    int key = 0;
    int releaseTab[4][4] = {  
   {0, 0, 0, 0} ,   
   {0, 0, 0, 0} ,   
   {0, 0, 0, 0} ,
   {0, 0, 0, 0}
};
    int release = 0;
    int compteur = 0;
    
    for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE lecture du clavier
    {
        ActiveColonne(colonne);
        for(ligne = 1;ligne <=4;ligne=ligne + 1)
        {
            KeyBoard[ligne-1][colonne-1] = LireLigne(ligne);
        }
    }
    
    
    for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE pour v�rifier une seule touche
    {                                                   //appuyer a la fois
        for(ligne = 1;ligne <=4;ligne=ligne + 1)
        {
            key = key + KeyBoard[ligne-1][colonne-1];
        }
    }
    if(key != 1)
    {
            for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE pour remettre toute les
        {                                                       //touche a 0 si plus d'une 
            for(ligne = 1;ligne <=4;ligne=ligne + 1)            //touche appuyer
            {
                KeyBoard[ligne-1][colonne-1] = 0;
            }
        }   
    }
    else
    {
        release = 1;
            while(release >= 1)
        {
                release = 1;
            for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE RElecture du clavier (release)
                {
                    ActiveColonne(colonne);
                    for(ligne = 1;ligne <=4;ligne=ligne + 1)
                    {
                        releaseTab[ligne-1][colonne-1] = LireLigne(ligne);
                        release = release + releaseTab[ligne-1][colonne-1];
                    }
                }
            release = release - 1;
            if(release > 1)
            {
                    for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE pour remettre toute les
                {                                                       //touche a 0 si plus d'une 
                    for(ligne = 1;ligne <=4;ligne=ligne + 1)            //touche appuyer
                    {
                        KeyBoard[ligne-1][colonne-1] = 0;
                    }
                }   
            }
            __delay_ms(50);
            compteur = compteur +1;
            if(compteur == 30)
            {
                release = 0;
                for(colonne = 1;colonne <=4;colonne=colonne + 1)    //BOUCLE pour remettre toute les
                {                                                       //touche a 0 si plus d'une 
                    for(ligne = 1;ligne <=4;ligne=ligne + 1)            //touche appuyer
                    {
                        KeyBoard[ligne-1][colonne-1] = 0;
                    }
                }
            }

        }
    }
    __delay_ms(50);
   
}

void AfficherTouche(int KeyBoard[4][4])  //KeyBoard[Ligne][Colonne]
{
       
    if(KeyBoard[0][0]==1)
    {
        putStringLCD("1");
    }
    if(KeyBoard[0][1]==1)
    {
        putStringLCD("2");
    }
    if(KeyBoard[0][2]==1)
    {
        putStringLCD("3");
    }
    if(KeyBoard[0][3]==1)
    {
        putStringLCD("A");
    }
    if(KeyBoard[1][0]==1)
    {
        putStringLCD("4");
    }
    if(KeyBoard[1][1]==1)
    {
        putStringLCD("5");
    }
    if(KeyBoard[1][2]==1)
    {
        putStringLCD("6");
    }
    if(KeyBoard[1][3]==1)
    {
        putStringLCD("B");
    }
    if(KeyBoard[2][0]==1)
    {
        putStringLCD("7");
    }
    if(KeyBoard[2][1]==1)
    {
        putStringLCD("8");
    }
    if(KeyBoard[2][2]==1)
    {
        putStringLCD("9");
    }
    if(KeyBoard[2][3]==1)
    {
        putStringLCD("C");
    }
    if(KeyBoard[3][0]==1)
    {
        putStringLCD("F");
    }
    if(KeyBoard[3][1]==1)
    {
        putStringLCD("0");
    }
    if(KeyBoard[3][2]==1)
    {
        putStringLCD("E");
    }
    if(KeyBoard[3][3]==1)
    {
        putStringLCD("D");
    }
}

void ActiveColonne(int colonne)
{
    //tris 1 input pull-up VCC    0 output low gnd open drain
   switch(colonne) {

   case 1  :
      TRISE= 0b11111110; //Colonne 1 Open Drain
      
      break; /* optional */
	
   case 2  :
      TRISE= 0b11111101; //Colonne 2 Open Drain
      break; /* optional */
      
   case 3  :
      TRISE= 0b11111011; //Colonne 3 Open Drain
      break; /* optional */
	
   case 4  :
      TRISE= 0b11110111; //Colonne 4 Open Drain
      break; /* optional */
  
}
}

int LireLigne(int ligne)
{
    switch(ligne) {

   case 1  :
       return !X1;
      
      break; /* optional */
	
   case 2  :
      return !X2;
      break; /* optional */
      
   case 3  :
       return !X3;
      break; /* optional */
	
   case 4  :
       return !X4;
      break; /* optional */
  
}
}