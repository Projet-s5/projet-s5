% Code pour l'ensemble des �l�ments de la d�mo 1
% �quipe P4
% 28 f�vrier 2019
clc
clear all
close all

%% Autocorr�lation
[x,y] = load_signal('TEK0001.CSV');
T = x(2)-x(1);
Fe = 1/T;
N = length(y);

C = xcorr(y,y);
C_test = zeros(1,N);
for i = 1:N-1
    for j = 1:i
       C_test(j) = C_test(j) + y(i-j+1)*y(i);
    end
end

C_for = zeros(1,2*N-1);
C_for(1:N) = C_test(N:-1:1);
C_for(N:2*N-1) = C_test(1:N);

figure;
subplot(3,1,1);
stem(C);
title('xcorr');
subplot(3,1,2);
stem(C_for);
title('autocorrelation avec boucle for');
subplot(3,1,3);
stem(C-C_for');
title('Dff�rence entre xcorr et la boucle');

% Version autocorrelation avec une fonction
%Par Charles-Frederick
clc
close all

Fe = 10000;
t = [1:1000];
N1 = length(t);
T = 1/Fe;
y = sin(pi/2*t*T);

C = xcorr(y,y);

[ResCorr, N] = ManCorr(y,y);

figure;
subplot(3,1,1);
stem(C);
title('xcorr');
subplot(3,1,2);
stem(ResCorr);
title('autocorrelation avec boucle for');
subplot(3,1,3);
stem(C-ResCorr);
title('Dff�rence entre xcorr et la boucle');

% Version correlation avec une fonction

clc
close all
clear all

Fe = 10000;
n_a = [1:1000];
N1 = length(n_a);
T = 1/Fe;

y1 = sin(pi/2*n_a*T);
y2 = [zeros(1,length(n_a)) y1 zeros(1,length(n_a))];
%y1 = [1 2 3 4 5 6 7 8]
%y2 = [2 3 4 5 6 7 8 9]
C = xcorr(y1,y2);

[CorrVector, N] = ManCorr(y1,y2);

figure;
subplot(3,1,1);
stem(C);
title('xcorr');
subplot(3,1,2);
stem(CorrVector);
title('autocorrelation avec boucle for');
subplot(3,1,3);
stem(C-CorrVector);
title('Dff�rence entre xcorr et la boucle');

%% Trame 

signal1 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]; % exemple
% 1 == pour capteur de son 
% 2 == pour la temperature
% 3 == pour le capteur de vibration #1
% 4 == pour le capteur de vibration #2
type = input('Entrer une valeur: ')
Trame = Tram(signal1,type);


%% FFT tests
clc
clear all
close all

%Fe = 10000;
%n = [1:1000];
%N1 = length(n);
%T = 1/Fe;
%y = sin(pi/2*n*T);

fs = 8000;
Fe = 24000;
t = 0:1/Fe:(10-1/Fe);

x = cos(2*pi*fs*t) + 10*cos(2*pi*(3333)*t) + 2.5*gallery('normaldata',size(t),4); % Gaussian noise;;
y = fft(x);
t = length(x);          % number of samples
fr = (0:t-1)*(fs/t);     % frequency range
power = abs(y).^2/t;    % power of the DFT

figure()
plot(power)
xlabel('Frequency')
ylabel('Power')

figure()
plot(x)

%% FFT processing of subsampled data

clc
close all 
clear all

%input = csvread('micro_Baseline_24khz.csv');
%input = csvread('micro_8kHz_1_24khz.csv');
input = csvread('micro_noise_24khz.csv');
sizeFFT = 256;
x1 = input(1:sizeFFT,1);
y1 = input(1:sizeFFT,2);

figure()
plot(x1,y1)

%inputFFT = 0;%(2*length(x));
%for i = 1:length(x)
%    inputFFt(2*i) = y(i);
%end

% Ajouter le signal 8kHz
Fe = 24000;
FreqSig= 8000; %Hz
%GainSig = 0.02;
%t = 0:1023;
y2 = y1';% + GainSig*cos(2*pi*FreqSig/Fe*t);% + 10*cos(2*pi*(3333)/Fe*t);

figure()
plot(x1,y2)

L = length(x1);
y3 = fft(y2);

P2 = abs(y3/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = 24000*(0:(L/2))/L;
plot(f,P1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')

%Detection de 8kHz en comparant le max entre 7.5k et 8.5k avec le avg.
avgVal = mean(P1);
maxVal = max(P1(floor((FreqSig-500)/(Fe/2)*(L/2)):ceil((FreqSig+500)/(Fe/2)*(L/2))))
if((avgVal > maxVal) || (maxVal < 1.5*avgVal))
    display('no break')
else
    display('has a break')
end
%%
clc
clear all
close all

fs = 8000;
Fe = 24000;
t = 0:127;

signal = cos(2*pi*fs/Fe*t) + 10*cos(2*pi*(3333)/Fe*t);% + 2.5*gallery('normaldata',size(t),4); % Gaussian noise;;

L=length(signal);
Y = fft(signal);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = 24000*(0:(L/2))/L;
plot(f,P1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')