function NewTab = Tram(tableau,type)


switch type
    case 1 % 1 == pour capteur de son 
        %nbEch = 16000; % 4 khz -> fe = 8 kHz -> deux periode donc 16000 echantions
        nbEch = 5;
       for i = 1:nbEch
             NewTab(i) = tableau(i);
       end
        
    case 2 % 2 == pour la temperature
        %nbEch = 40; % 10 hz -> fe = 20 -> deux periode donc 40
        nbEch = 3;
       for i = 1:nbEch
             NewTab(i) = tableau(i);
       end
    case 3 % 3 == pour les capteur de vibrations piezoelectric
        %nbEch = 720; % 180 hz -> fe = 360 -> deux periode donc 720
        nbEch = 17;
       for i = 1:nbEch
             NewTab(i) = tableau(i);
       end 
    case 4 % 4 == pour le capteur de vibration #2
        %nbEch = 3; % a determiner
        nbEch = 9;
       for i = 1:nbEch
             NewTab(i) = tableau(i);
       end
end

end
















