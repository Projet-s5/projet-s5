/*
 * Interface.c
 *
 *  Created on: Mar 17, 2019
 *      Author: charles-frederick
 */

#include "../../includes/Interface.h"

void outputCorrelation(int* sig1, unsigned length1, char* filepath)//, int* sig2, unsigned length2, int* sig3, unsigned length3, int* sig4, unsigned length4)
{
    //Ouverture des 4 fichiers de output
    FILE* hFile1 = fopen(filepath,"w+");

    outputTableToFile(hFile1, length1, sig1);

    //Close the files
    fclose(hFile1);
}
/*
void outputTableToFile(FILE* hFile, unsigned tableLength, int* table)
{
    fprintf(hFile, "\nSignal1:\n");
    unsigned i = 0;
    for(; i < tableLength; i++)
    {
        fprintf(hFile, "%d",table[i]);
    }
}
*/

void outputTableToFile(FILE* hFile, unsigned tableLength, int* table)
{
    unsigned i = 0;
    for(; i < tableLength; i++)
    {
        fprintf(hFile, "%d,",table[i]);
    }
}

void appendToFile(int* sig1, unsigned length1, char* filepath)
{
    unsigned i = 0;
    FILE* hFile = fopen(filepath,"w");
    fprintf(hFile, "\n",sig1[i]);
    for(; i < length1; i++)
    {
        fprintf(hFile, "%d,",sig1[i]);
    }
    fclose(hFile);
}
