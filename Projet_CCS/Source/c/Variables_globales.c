/*
 * Variables_global.c
 *
 *  Created on: 31 mars 2019
 *      Author: pels2511
 */

#include "../../includes/Variables_globales.h"

//variable globale

// variables pour le son
int bufferSon[TAILLE_SON] = {0};
int tableauSon[TAILLE_SON] = {0};
int tableauSonReference[TAILLE_SON] = {0};
unsigned short indexSon = 0;
unsigned short sonComplete = FAUX;
unsigned short sonReferenceOK = FAUX;


// variables pour la vibration
int bufferVibration[TAILLE_VIBRATION] = {0};
int tableauVibration[TAILLE_VIBRATION] = {0};
int tableauVibrationReference[TAILLE_VIBRATION] = {0};
unsigned short indexVibration = 0;
unsigned short vibrationComplete = FAUX;
unsigned short vibrationReferenceOK = FAUX;
unsigned short timerVibration = 0;


// variables pour la temperature
int bufferTemperature = 0;
int temperature = 0;
int temperatureReference = 0;
unsigned short temperatureComplete = FAUX;
unsigned short temperatureReferenceOK = FAUX;
unsigned short timerTemperature = 0;


// autres variables
unsigned short etablirValeursReferences = FAUX;
extern unsigned short commencerProgramme = FAUX;
unsigned int message = 0;
unsigned int messageRecu = FAUX;
unsigned short moteurMarche = FAUX;
































