/*
 * Traitement_vibration.c
 *
 *  Created on: 31 mars 2019
 *      Author: pels2511
 */

#include "../../includes/Traitement_vibration.h"


//Changer si on veut utiliser plus/moins de trames pour calibrer la correlation de vibration
#define NOMBRE_TRAME_CALIBRATION 4

#pragma DATA_ALIGN(trameRef,256);
#pragma DATA_ALIGN(currentCorr,512);
unsigned long currentSum = 0;
unsigned currentMax = 0;
unsigned long currentAvg = 0;
unsigned short currentID = 0;

int trameRef[128];
int currentCorr[256];

bool VibrationCalib = false;
bool CalibStarted = false;

unsigned short DetecterNiveauVibration(int* sig1,  unsigned length1)
{
    unsigned returnVal = 0;
    if((!VibrationCalib) || (CalibStarted))
    {
        returnVal = ERROR_CORR;
        return returnVal;
    }

    //Ready the result array.
    unsigned lengthRes = 0;//(unsigned*) malloc(1); //bug
    ASMCorrCall(sig1,length1,trameRef,128,currentCorr, &lengthRes);

    //Treat the result.
    //We do a thresholding of the output data relative to the reference max *** TO BE MODIFIED ***
    int CorrMaxVal = CorrMax(currentCorr,lengthRes);
    unsigned long CorrAvgVal = CorrAvg(currentCorr,lengthRes);
    //double val1 = ((double)CorrMaxVal/(double)CorrAvgVal);
    //double val2 = ((double)currentMax/(double)currentAvg);
    //if(val1 > (1.2*val2))
    if(CorrMaxVal > (2*currentMax))
    {
        returnVal = NIVEAU_CRITIQUE;//NIVEAU_CRITIQUE;
    }
    else
    {
        returnVal = NIVEAU_NORMAL;
    }
    //printf("returnVal = %d\n",returnVal);

//#ifdef DEBUG_CORR
    //Output to file
    //outputCorrelation(corrRes,lengthRes, filePath1);
    //printf("\nCorrValMax = %d", CorrMaxVal);
    //printf("\nCORR_THRESHOLD_MAX = %d", CORR_THRESHOLD_MAX);
    //printf("\nCORR_THRESHOLD_MIN = %d", CORR_THRESHOLD_MIN);
//#endif
    return returnVal;

    //return NIVEAU_NORMAL;
}

bool VibrationEstCalibre()
{
    return (VibrationCalib && !CalibStarted);
}

bool StartVibrationCalibration()
{
    CalibStarted = true;
    VibrationCalib = false;
    return true;
}

bool CalibrerVibration(int* trameCalib)
{
    if(CalibStarted)
    {
        //Continue the calibration
        unsigned lengthRes = 0;
        ASMCorrCall(trameCalib,128,trameRef,128,currentCorr, &lengthRes);
        //Acquire max, and sum to get avg
        unsigned corrMaxVal =CorrMax(currentCorr,128);
        if(corrMaxVal > currentMax)
        {
            currentMax = corrMaxVal;
        }
        currentSum += CorrSum(currentCorr,128);
        if(currentID == NOMBRE_TRAME_CALIBRATION)
        {
            //Complete the calibration and define the thresholds
            currentID = 1;
            currentAvg = currentSum/((NOMBRE_TRAME_CALIBRATION-1)*128);
            etablirValeursReferences = FAUX;
            VibrationCalib = true;
            CalibStarted = false;
        }
    }
    else if(currentID > 0)
    {
        //Start the calibration
        unsigned i;
        for(i = 0; i < 128; i++)
        {
            trameRef[i] = trameCalib[i];
        }
        //unsigned lengthRes;
        //ASMCorrCall(trameRef,128,trameRef,128,currentCorr, &lengthRes);

        //unsigned corrMaxVal =CorrMax(currentCorr,128);
        //if(corrMaxVal > currentMax)
        //{
        //    currentMax = corrMaxVal;
        //}

        //currentSum += CorrSum(currentCorr,128);
        StartVibrationCalibration();
    }
    currentID++;
    return true;
}

int VibrationCorrProcess(int* sig1,  unsigned length1, int* sig2, unsigned length2, int* corrRes)
{
    unsigned returnVal = 0;
    if(length1 != length2)
        returnVal = ERROR_CORR;

    //Ready the result array.
    unsigned lengthRes = 0;//(unsigned*) malloc(1); //bug

    ASMCorrCall(sig1,length1,sig2,length2,corrRes, &lengthRes);

    //Treat the result.
    //We do a thresholding of the output data relative to the reference max *** TO BE MODIFIED ***
    int CorrMaxVal = CorrMax(corrRes,lengthRes);
    if(CorrMaxVal > CORR_THRESHOLD_MAX || CorrMaxVal < CORR_THRESHOLD_MIN)
    {
        returnVal = OUTSIDE_CORR_THRESHOLD;
    }
    else
    {
        returnVal = INSIDE_CORR_THRESHOLD;
    }
    printf("CorrMaxVal = %d\n",CorrMaxVal);

//#ifdef DEBUG_CORR
    //Output to file
    //outputCorrelation(corrRes,lengthRes, filePath1);
    //printf("\nCorrValMax = %d", CorrMaxVal);
    //printf("\nCORR_THRESHOLD_MAX = %d", CORR_THRESHOLD_MAX);
    //printf("\nCORR_THRESHOLD_MIN = %d", CORR_THRESHOLD_MIN);
//#endif
    return returnVal;
}
