/* Fonction de fenetrage pour les trames (Hamming)
 * 4 avril 2019
 * Par : lapj2123
 */

#include "../../includes/Fenetre.h"
#include "../../includes/Hammingcoeffs.dat"

void fenetreHamming(int *Trame, short N)
{
    short i = 0;

    if(N == 128)
    {
        for (i=0;i<128;i++)
        {
            Trame[i] *= (int)Hammingcoeffs[i*2];
        }
    }
    else if(N == 256)
    {
        for (i=0;i<256;i++)
        {
            Trame[i] *= (int)Hammingcoeffs[i];
        }
    }
}
