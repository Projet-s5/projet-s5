/*
 * main.c
 */

#include <stdio.h>
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support
#include "../../includes/Fonction_projet.h" //codec support

#include "../../includes/Correlation.h"
#include "../../includes/Interface.h"
#include "../../includes/Tableau_Vibration.h"
#include "../../includes/FFT.h"
#include "../../includes/TableauSignaux.h"
#include "../../includes/Audio_driver.h"

#include "../../includes/Traitement_temperature.h"
#include "../../includes/Traitement_vibration.h"
#include "../../includes/Traitement_son.h"
#include "../../includes/SPI_driver.h"


#include <csl_mcbsphal.h>

#include <csl.h>
#include <csl_gpio.h>
#include <csl_mcbsp.h>
#include <stdio.h>
#include <csl_irq.h>
#include <dsk6713_led.h>
#include <dsk6713_aic23.h>
#include <dsk6713.h>

//Enlever le define si on ne veut pas debug l'autocorr.
#define DEBUG_CORR 0x99

//float signal[TAILLE_SON];
//float valeurConversion[TAILLE_SON];
//int messageRecu = 0;
int valeurMessage = 0;
int EtablirValeurReference = 0;
//int sonComplete;

unsigned mode = 0;
/*
 * mode 0 = mode test
 * mode 1 = test DSK
 */

#pragma DATA_ALIGN(trame_vibration_ref,1024);
//#pragma DATA_ALIGN(trame_vibration_ref2,256);
#pragma DATA_ALIGN(corrRes_ref,64);


int i = 0;

unsigned long128 = 128;
unsigned long256 = 256;
short curseur_vibration_ref = 0;
short curseur_vibration_ref2 = 0;
short curseur_vibration_bruit1 = 0;
short curseur_vibration_bruit2 = 0;
short curseur_son_8kHz1 = 768;
int trame_vibration_ref[128];
int trame_vibration_bruit1[128];
int trame_vibration_bruit2[128];
int trame_son_8kHz1[256];
int corrRes_bruit1[255];
int corrRes_bruit2[255];

int corrRes_ref[255];
int corrRes_ref2[255];
int corrRes_current[255];

short bufferCritiqueSon = 0;

int main(void) {

    DSK6713_init(); // Initialize the board support library
    Audio_init();
    DSK6713_waitusec(100000);
    SPI_init();

    unsigned short niveauTemperature = NIVEAU_NORMAL;
    unsigned short niveauVibration = NIVEAU_NORMAL;
    unsigned short niveauSon = NIVEAU_NORMAL;


    /*
    while(1) // boucle de test pour la telecommande
    {
        if(messageRecu == VRAI)
        {
            traduireMessage(RS232_data_in);
            printf("message recu: %#X\n",RS232_data_in);
            messageRecu = FAUX;
            DSK6713_waitusec(100000);
        }
        RS232_data_out = 0x95;
        ecrire_MCBSP();
        DSK6713_waitusec(100000);
        DSK6713_waitusec(100000);
        DSK6713_waitusec(100000);
    }
    */



    while(1)
    {

        if(messageRecu == VRAI)
        {
            traduireMessage(RS232_data_in);
            messageRecu = FAUX;
        }

        if((vibrationReferenceOK==VRAI)&&((etablirValeursReferences == VRAI)))
        {
            unsigned i;
            for(i = 0; i < TAILLE_VIBRATION;i++)
            {
                trame_vibration_ref[i] = tableauVibrationReference[i];
            }
            CalibrerVibration(trame_vibration_ref);
            vibrationReferenceOK = FAUX;
        }

        else if((moteurMarche == VRAI) && (etablirValeursReferences == FAUX) && (commencerProgramme == VRAI))
        {

            if(temperatureComplete == VRAI)
            {
                CopierBufferTemperaure();
                temperatureComplete = FAUX;
                niveauTemperature = DetecterNiveauTemperature();


                RS232_data_out = traduireEnvoi(RS232_data_out, niveauTemperature, MESURE_TEMPERATURE);
                if(niveauTemperature == NIVEAU_CRITIQUE)
                {
                    ArreterMoteur();
                }

            }
            if(vibrationComplete == VRAI)
            {
                CopierBufferVibration();
                vibrationComplete = FAUX;
                niveauVibration = DetecterNiveauVibration(tableauVibration,TAILLE_VIBRATION);


                RS232_data_out = traduireEnvoi(RS232_data_out, niveauVibration, MESURE_VIBRATION);
                if(niveauVibration == NIVEAU_CRITIQUE)
                {
                    ArreterMoteur();
                }
            }
            if(sonComplete == VRAI)
            {
                CopierBufferSon();
                sonComplete = FAUX;
                niveauSon = DetecterNiveauSon();

                RS232_data_out = traduireEnvoi(RS232_data_out, niveauSon, MESURE_SON);
                if(niveauSon == NIVEAU_CRITIQUE)
                {
                    if(bufferCritiqueSon == 0)
                    {
                        bufferCritiqueSon = 1;
                    }
                    else
                    {
                        ArreterMoteur();
                    }

                }

            }
        }

        if(messageAEnvoyer == VRAI && MCBSP_xrdy(DSK6713_AIC23_CONTROLHANDLE))
        {
            ecrire_MCBSP();
            messageAEnvoyer = FAUX;
        }


    }
	return 0;
}
