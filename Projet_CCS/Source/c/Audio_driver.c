/********************************************************
**  Session 5 - APP6 - Téléphonie par DSP
**  Fichier Audio_driver.c
**  Auteurs : < vos noms >
**  Date : < derniere modification >
********************************************************/

#define AUDIO_DRIVER_MODULE_IMPORT
#include "../../includes/Audio_driver.h"
#include "../../includes/filtreFIRcoeffs.dat"

extern far void vectors();   // Vecteurs d'interruption

#define TAMPON_L  32
#pragma DATA_ALIGN(tampon, TAMPON_L*2); // Requis pour l'adressage circulaire en assembleur
short tampon[TAMPON_L]={0};         // Tampon d'échantillons
short *pTampon=&tampon[TAMPON_L-1]; // Pointeur sur l'échantillon courant

short compteurMessage = 0;
unsigned short messageAEnvoyer = FAUX;


void Audio_init(void)
{
    comm_intr(DSK6713_AIC23_FREQ_24KHZ,DSK6713_AIC23_INPUT_LINE);
    //IRQ SETUP
    //IRQ_setVecs(vectors);     /* point to the IRQ vector table    */
    //IRQ_map(IRQ_EVT_GPINT4,4);
    //IRQ_reset(IRQ_EVT_GPINT4);
    //IRQ_enable(IRQ_EVT_GPINT4);       /* Globally enable interrupts       */
    //IRQ_nmiEnable();          /* Enable NMI interrupt             */
    //IRQ_globalEnable();          /* Enable NMI interrupt             */

	return;
}


interrupt void c_int11(void)
{
    compteurMessage++;
    if(compteurMessage >= TAILLE_SON)
    {
        messageAEnvoyer = VRAI;
        compteurMessage = 0;
    }

    if(moteurMarche == VRAI)
    {
        timerVibration++;

        short vibration = input_right_sample();
        short son = input_left_sample();
        ReceptionValeurSon(son);

        if(timerVibration >= TEMPS_VIBRATION)
        {
            timerVibration = 0;
            timerTemperature++;
            ReceptionValeurVibration(vibration);
        }
        if(timerTemperature >= TEMPS_TEMPERATURE)
        {
            timerTemperature = 0;
            DemanderLectureTemperature();
        }

        output_left_sample(son);
        output_right_sample(vibration);
        return;
    }
    else
    {
        output_sample(0);
        return;
    }


}

void ReceptionValeurSon(short son)
{
    short valeurRetour;
    if(etablirValeursReferences==VRAI)
    {
        pTampon = filtreFIR(pTampon, son, CoeffsFIR, &valeurRetour);
        tableauSonReference[indexSon] = (int)valeurRetour;

        indexSon++;

        if(indexSon>=TAILLE_SON)
        {
            indexSon = 0;
            sonReferenceOK = VRAI;
        }
    }
    else if(commencerProgramme == VRAI)
    {
        pTampon = filtreFIR(pTampon,son, CoeffsFIR, &valeurRetour);
        bufferSon[indexSon] = (int)valeurRetour;

        bufferSon[indexSon] = son;
        indexSon++;

        if(indexSon>=TAILLE_SON)
        {
            indexSon = 0;
            sonComplete = VRAI;
        }

    }
}

void ReceptionValeurVibration(short vibration)
{
    if(etablirValeursReferences==VRAI)
    {
        tableauVibrationReference[indexVibration] = (int)vibration;
        indexVibration++;

        if(indexVibration>=TAILLE_VIBRATION)
        {
            indexVibration = 0;
            vibrationReferenceOK = VRAI;
        }
    }
    else if(commencerProgramme == VRAI)
    {
        bufferVibration[indexVibration] = vibration;
        indexVibration++;

        if(indexVibration>=TAILLE_VIBRATION)
        {
            indexVibration = 0;
            vibrationComplete = VRAI;
        }
    }
}

void DemanderLectureTemperature()
{
    //implémenter le I2C
}


void CopierBufferTemperaure()
{
    temperature = bufferTemperature;
}

void CopierBufferSon()
{
    int i;
    for(i=0;i<TAILLE_SON;i++)
    {
        tableauSon[i] = bufferSon[i];
    }
}

void CopierBufferVibration()
{
    int i;
    for(i=0;i<TAILLE_VIBRATION;i++)
    {
        tableauVibration[i] = bufferVibration[i];
    }
}




// end of Audio_driver.c
