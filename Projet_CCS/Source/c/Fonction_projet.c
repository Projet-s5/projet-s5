/*
 * Fonction
 */

//Headers

#include "../../includes/Fonction_projet.h"

//

//variable globale
//int moteurMarche = 0;
float tab_ref[4] = {2,2,2,2}; // Grandeur arbitraire
float tab_ref[4]; // Grandeur arbitraire
//

//definition de fonction

void enableInterrupts(void); //fonction asm

void LED0(void){
    DSK6713_rset(DSK6713_USER_REG, 0x1); //allume leds 0
}

void LED1(void){
    DSK6713_rset(DSK6713_USER_REG, 0x2); //allume leds 1
}

void LED2(void){
    DSK6713_rset(DSK6713_USER_REG, 0x4); //allume leds 2
}

void LED3(void){
    DSK6713_rset(DSK6713_USER_REG, 0x8); //allume leds 3
}

void EteindreTemoinLumineux()
{
    DSK6713_rset(DSK6713_USER_REG, 0x0); //eteint les leds
}

void traduireMessage(int valeurMessage)
{

    if (valeurMessage == ARRETER)
    {
         ArreterMoteur();
    }
    if (valeurMessage == DEMARRER)
    {
        DemarrerMoteur();
    }
    if(valeurMessage == REFERENCE)
    {
        etablirValeursReferences = VRAI;
        commencerProgramme = VRAI;
    }

}

void DemarrerMoteur()
{
    //implementer le signal permettant le demarage du moteur
    Uint8 stateRelais = DSK6713_rget(DSK6713_DC_REG);
    stateRelais = stateRelais|MASK_RELAIS;
    DSK6713_rset(DSK6713_DC_REG, stateRelais);

    RS232_data_out = (1<<7) | RS232_data_out;
    RS232_data_out = ~(1<<6) & RS232_data_out;
    ecrire_MCBSP();

    moteurMarche = VRAI;

}

void ArreterMoteur()
{
    //implementer le signal permettant l arret du moteur
    moteurMarche = FAUX;

    Uint8 stateRelais = DSK6713_rget(DSK6713_DC_REG);
    stateRelais = stateRelais & ~MASK_RELAIS; //stateRelais & (~MASK_RELAIS);
    DSK6713_rset(DSK6713_DC_REG, stateRelais);

    RS232_data_out = ~(1<<7) & RS232_data_out;
    RS232_data_out = (1<<6) | RS232_data_out;
    ecrire_MCBSP();
}


unsigned int traduireEnvoi(unsigned int messageCourant, unsigned short niveauMesure, unsigned short decalage)
{

    niveauMesure   = (niveauMesure) << decalage; // Placer le message sur les bon bits (b0b1 pour son, b2b3 pour vibration b4b5 pour temperature)
    unsigned int mask = (3 << decalage);
    messageCourant = (niveauMesure)  | messageCourant;   // �crire les 1

    niveauMesure = niveauMesure ^ mask;

    messageCourant = (~niveauMesure) & messageCourant;  // �crire les 0





    return messageCourant;
}






