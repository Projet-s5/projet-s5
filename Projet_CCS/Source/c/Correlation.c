/*
 * Correlation.c
 */

//Headers

#include "../../includes/Correlation.h"


void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes)
{
    // Pad the signals if the length is not equal
    if(length1 != length2)
    {
        if(length1 > length2)
        {
            //Pad the sig2
            length2 = length1;
        }
        else
        {
            //Pad the sig1
            length1 = length2;
        }
    }

    //Reset to 0 all the results
    *sizeRes = length1+length2-1;

    unsigned i;
    for (i=0; i<(*sizeRes) ; i++)
      {
        corrRes[i] = 0;
      }

    //corrRes = (short*) calloc(*sizeRes, sizeof(short));
    ASMCorr(sig1,length1,sig2,length2,corrRes);
}


int CorrMax(int* sig, unsigned lengthSig)
{
    int max, i;
    for(i = 0, max = 0; i < lengthSig; i++)
    {
        if(sig[i] > max)
            max = sig[i];
    }
    return max;
}

unsigned long CorrAvg(int* sig, unsigned lengthSig)
{
    unsigned long avg;
    unsigned i;
    for(i = 0, avg = 0; i < lengthSig; i++)
    {
        avg += abs(sig[i]);
    }
    return avg/lengthSig;
}

unsigned long CorrSum(int* sig, unsigned lengthSig)
{
    unsigned long sum;
    unsigned i;
    for(sum = 0, i = 0; i < lengthSig; i++)
    {
        sum += abs(sig[i]);
    }
    return sum;
}

int corrRes__ref1[255];
int corrRes__ref2[255];
int corrRes__ref3[255];
int corrRes__bruit1[255];
int corrRes__bruit2[255];


/*
void ThresholdIdentification()
{
    //Fonction d'identification des threshold de correlation de la vibration. Comparaison de l'ensemble des trames.
    short curseur_vibration_ref1 = 0;
    short curseur_vibration_ref2 = 0;
    short curseur_vibration_ref3 = 0;
    short curseur_vibration_bruit1 = 0;
    short curseur_vibration_bruit2 = 0;

    //unsigned const nbSig = 5;
    //unsigned const nbTrames = floor(1750/128);
    unsigned resTable[20][13][2];
    unsigned i;
    for(i = 0; i < 20; i++)
    {
        curseur_vibration_ref1 = Tram(piezo1_baseline, 1, curseur_vibration_ref1, corrRes__ref1);
        curseur_vibration_ref2 = Tram(piezo2_baseline, 1, curseur_vibration_ref2, corrRes__ref2);
        curseur_vibration_ref3 = Tram(piezo3_baseline, 1, curseur_vibration_ref3, corrRes__ref3);
        curseur_vibration_bruit1 = Tram(piezo1_noise, 1, curseur_vibration_bruit1, corrRes__bruit1);
        curseur_vibration_bruit2 = Tram(piezo2_noise, 1, curseur_vibration_bruit2, corrRes__bruit2);

        unsigned lengthRes = 0;
        //ref1 avec ref2
        ASMCorrCall(corrRes__ref1,128,corrRes__ref2,128,corrRes__ref1, &lengthRes);
        resTable[0][i][0] = CorrMax(corrRes__ref1,128);
        resTable[0][i][1] = CorrAvg(corrRes__ref1,128);

        //ref1 avec ref3
        ASMCorrCall(corrRes__ref1,128,corrRes__ref3,128,corrRes__ref1, &lengthRes);
        resTable[1][i][0] = CorrMax(corrRes__ref1,128);
        resTable[1][i][1] = CorrAvg(corrRes__ref1,128);

        //ref1 avec bruit1
        ASMCorrCall(corrRes__ref1,128,corrRes__bruit1,128,corrRes__bruit1, &lengthRes);
        resTable[2][i][0] = CorrMax(corrRes__ref1,128);
        resTable[2][i][1] = CorrAvg(corrRes__ref1,128);

        //ref1 avec bruit2
        ASMCorrCall(corrRes__ref1,128,corrRes__bruit2,128,corrRes__bruit1, &lengthRes);
        resTable[3][i][0] = CorrMax(corrRes__ref1,128);
        resTable[3][i][1] = CorrAvg(corrRes__ref1,128);

        //ref2 avec ref1
        ASMCorrCall(corrRes__ref2,128,corrRes__ref1,128,corrRes__ref2, &lengthRes);
        resTable[4][i][0] = CorrMax(corrRes__ref2,128);
        resTable[4][i][1] = CorrAvg(corrRes__ref2,128);

        //ref2 avec ref3
        ASMCorrCall(corrRes__ref2,128,corrRes__ref3,128,corrRes__ref2, &lengthRes);
        resTable[5][i][0] = CorrMax(corrRes__ref2,128);
        resTable[5][i][1] = CorrAvg(corrRes__ref2,128);

        //ref2 avec bruit1
        ASMCorrCall(corrRes__ref2,128,corrRes__bruit1,128,corrRes__ref2, &lengthRes);
        resTable[6][i][0] = CorrMax(corrRes__ref2,128);
        resTable[6][i][1] = CorrAvg(corrRes__ref2,128);

        //ref2 avec bruit2
        ASMCorrCall(corrRes__ref2,128,corrRes__bruit2,128,corrRes__ref2, &lengthRes);
        resTable[7][i][0] = CorrMax(corrRes__ref2,128);
        resTable[7][i][1] = CorrAvg(corrRes__ref2,128);

        //ref3 avec ref1
        ASMCorrCall(corrRes__ref3,128,corrRes__ref1,128,corrRes__ref3, &lengthRes);
        resTable[8][i][0] = CorrMax(corrRes__ref3,128);
        resTable[8][i][1] = CorrAvg(corrRes__ref3,128);

        //ref3 avec ref2
        ASMCorrCall(corrRes__ref3,128,corrRes__ref2,128,corrRes__ref3, &lengthRes);
        resTable[9][i][0] = CorrMax(corrRes__ref3,128);
        resTable[9][i][1] = CorrAvg(corrRes__ref3,128);

        //ref3 avec bruit1
        ASMCorrCall(corrRes__ref3,128,corrRes__bruit1,128,corrRes__ref3, &lengthRes);
        resTable[10][i][0] = CorrMax(corrRes__ref3,128);
        resTable[10][i][1] = CorrAvg(corrRes__ref3,128);

        //ref3 avec bruit2
        ASMCorrCall(corrRes__ref3,128,corrRes__bruit2,128,corrRes__ref3, &lengthRes);
        resTable[11][i][0] = CorrMax(corrRes__ref3,128);
        resTable[11][i][1] = CorrAvg(corrRes__ref3,128);

        //bruit1 avec ref1
        ASMCorrCall(corrRes__bruit1,128,corrRes__ref1,128,corrRes__bruit1, &lengthRes);
        resTable[12][i][0] = CorrMax(corrRes__bruit1,128);
        resTable[12][i][1] = CorrAvg(corrRes__bruit1,128);

        //bruit1 avec ref2
        ASMCorrCall(corrRes__bruit1,128,corrRes__ref2,128,corrRes__bruit1, &lengthRes);
        resTable[13][i][0] = CorrMax(corrRes__bruit1,128);
        resTable[13][i][1] = CorrAvg(corrRes__bruit1,128);

        //bruit1 avec ref3
        ASMCorrCall(corrRes__bruit1,128,corrRes__ref3,128,corrRes__bruit1, &lengthRes);
        resTable[14][i][0] = CorrMax(corrRes__bruit1,128);
        resTable[14][i][1] = CorrAvg(corrRes__bruit1,128);

        //bruit1 avec bruit2
        ASMCorrCall(corrRes__bruit1,128,corrRes__bruit2,128,corrRes__bruit1, &lengthRes);
        resTable[15][i][0] = CorrMax(corrRes__bruit1,128);
        resTable[15][i][1] = CorrAvg(corrRes__bruit1,128);

        //bruit2 avec ref1
        ASMCorrCall(corrRes__bruit2,128,corrRes__ref1,128,corrRes__bruit2, &lengthRes);
        resTable[16][i][0] = CorrMax(corrRes__bruit2,128);
        resTable[16][i][1] = CorrAvg(corrRes__bruit2,128);

        //bruit2 avec ref2
        ASMCorrCall(corrRes__bruit2,128,corrRes__ref2,128,corrRes__bruit2, &lengthRes);
        resTable[17][i][0] = CorrMax(corrRes__bruit2,128);
        resTable[17][i][1] = CorrAvg(corrRes__bruit2,128);

        //bruit2 avec ref3
        ASMCorrCall(corrRes__bruit2,128,corrRes__ref3,128,corrRes__bruit2, &lengthRes);
        resTable[18][i][0] = CorrMax(corrRes__bruit2,128);
        resTable[18][i][1] = CorrAvg(corrRes__bruit2,128);

        //bruit2 avec bruit1
        ASMCorrCall(corrRes__bruit2,128,corrRes__bruit1,128,corrRes__bruit2, &lengthRes);
        resTable[19][i][0] = CorrMax(corrRes__bruit2,128);
        resTable[19][i][1] = CorrAvg(corrRes__bruit2,128);
    }
}

*/

/*
void Test()
{
    //Tester la correlation
    unsigned longeurCAmpRef = sizeof(tableauVibrationReference)/sizeof(float);
    unsigned longeurCAmpBruit1 = sizeof(tableauVibration1)/sizeof(float);
    unsigned longeurCAmpBruit2 = sizeof(tableauVibration2)/sizeof(float);
    unsigned longeurCAmpBruit3 = sizeof(tableauVibration3)/sizeof(float);

    //int correlRef[2500];
    int* correlRef = (int*) malloc(2500);
    unsigned lengthRef;
    //int correl1[2500];
    int* correl1 = (int*) malloc(2500);
    unsigned length1;
    //int correl2[2500];
    int* correl2 = (int*) malloc(2500);
    unsigned length2;
    //int correl3[2500];
    int* correl3 = (int*) malloc(2500);
    unsigned length3;

    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibrationReference, longeurCAmpRef, correlRef, &lengthRef);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration1, longeurCAmpBruit1, correl1, &length1);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration2, longeurCAmpBruit2, correl2, &length2);
    ASMCorrCall(tableauVibrationReference, longeurCAmpRef, tableauVibration3, longeurCAmpBruit3, correl3, &length3);

    //char* outputPath = "";
    outputCorrelation4(correlRef,lengthRef);//, correl1, length1, correl2, length2, correl3, length3);

    //free(folderpath);
    free(correlRef);
    free(correl1);
    free(correl2);
    free(correl3);

}
*/
