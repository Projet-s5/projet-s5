/*
 * Filtre.c
 *
 *  Created on: Apr 12, 2019
 *      Author: charles-frederick
 */

#include "../../includes/Filtre.h"

//Retourne le pointeur tampon
short* filtreFIR63(short* pTampon, short tableauEntree, const short* coeffsFIR, short* valeurRetour)
{
    //Fonction shell si on veut faire plusieurs call de filtre differents.
    pTampon = filtreFIR(pTampon, tableauEntree, coeffsFIR, valeurRetour);
    return pTampon;
}
