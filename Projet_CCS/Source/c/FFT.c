/*
 * FFT.c
 *
 *  Created on: Apr 1, 2019
 *      Author: charles-frederick
 */



#include "../../includes/FFT.h"




#pragma DATA_ALIGN(fftRes,length256*4);
float fftRes[2*length256];


unsigned short FFTProcess(int* sig, unsigned lengthSig)
{
    unsigned short returnVal = 0;
    short index[16];
    //do the Appropriate FFT or return error.
    unsigned i = 0;
    switch(lengthSig)
    {
        case length256:
        {
            //Case of 256 ech input signal

            for(i=0; i < length256; i++)
            {
                fftRes[2*i] = (float)sig[i];
                fftRes[2*i+1] = 0.0;
            }

            DSPF_sp_cfftr2_dit(fftRes,w256,lengthSig);

            bitrev_index(index,lengthSig);

            DSPF_sp_bitrev_cplx((double*)fftRes, index,lengthSig);


            for(i=0;i<(lengthSig);i++)
            {
                fftRes[i] = fftRes[2*i]*fftRes[2*i] + fftRes[2*i+1]*fftRes[2*i+1];
            }
            for(i=lengthSig;i<(lengthSig*2);i++)
            {
                fftRes[i] = 0.0;
            }

            break;
        }
        default:
        {
            //Case of other random amount of ech input signals
            returnVal = ERROR_FFT;
            break;
        }
    }

    //Find the average of the fft
    int averageFFTValue = FFTAverage(fftRes,lengthSig);

    //Find the local max around the 8k freq

    unsigned normalizedStartID = floor(((FREQ_CENTER-FREQ_MARGIN)*lengthSig)/12000);
    unsigned normalizedEndID = floor(((FREQ_CENTER+FREQ_MARGIN)*lengthSig)/12000);
    int local8KHzMax = FFTMaxLocal(fftRes,lengthSig,normalizedStartID,normalizedEndID);

    //Do the fftAnalysis logic from matlab: condition for identification: if LocalMaxVal(8k) > 1.5*AvgVal
    if(local8KHzMax > (150*averageFFTValue))
    {
        returnVal = NIVEAU_CRITIQUE;
    }
    else
    {
        returnVal = NIVEAU_NORMAL;
    }
    return returnVal;

}

int FFTAverage(float* sig,unsigned lengthSig)//, unsigned startIndex, unsigned endIndex)
{
    int average, i;
    for(i = 0, average = 0; i < lengthSig; i++)
    {
        average = average + sig[i];
    }
    return average/lengthSig;
}

int FFTAverageLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int average, i;
        for(i = startID, average = 0; i < endID; i++)
        {
            average = average + sig[i];
        }
        return average/lengthSig;
    }
    else
        return ERROR_FFT;
}

int FFTAverageComplex(float* sig, unsigned lengthSig)
{
    int average, i;
    for(i = 0, average = 0; i < lengthSig; i++)
    {
        average = average + sqrt(sig[2*i]*sig[2*i]+sig[2*i+1]*sig[2*i+1]);
    }
    return average/(lengthSig);
}

int FFTAverageComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int average, i;
        for(i = startID, average = 0; i < endID/2; i++)
        {
            average = average + sqrt(sig[2*i]+sig[2*i+1]);
        }
        return average/(endID-startID)*2;
    }
    else
        return ERROR_FFT;
}

int FFTMax(int* sig, unsigned lengthSig)//, unsigned startIndex, unsigned endIndex)
{
    int max, i;
    for(i = 0, max = 0; i < lengthSig; i++)
    {
        if(sig[i] > max)
            max = sig[i];
    }
    return max;
}

int FFTMaxLocal(float* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int max, i;
        for(i = startID, max = 0; i < endID; i++)
        {
            if(sig[i] > max)
                max = sig[i];
        }
        return max;
    }
    else
        return ERROR_FFT;
}

int FFTMaxComplex(int* sig, unsigned lengthSig)
{
    int max, i;
    for(i = 0, max = 0; i < lengthSig/2; i++)
    {
        unsigned val = sqrt(sig[2*i]*sig[2*i+1]);
        if(sig[i] > max)
            max = sig[i];
    }
    return max;
}

int FFTMaxComplexLocal(float* sig, unsigned lengthSig, unsigned startID, unsigned endID)
{
    if(startID > 0 && endID < lengthSig)
    {
        int max, i;
        for(i = startID, max = 0; i < endID; i++)
        {
            unsigned val = sqrt(sig[2*i]*sig[2*i]+sig[2*i+1]*sig[2*i+1]);
            if(val > max)
                max = val;
        }
        return max;
    }
    else
        return ERROR_FFT;
}


void bitrev_index(short *index, int nx)
{
    int i, j, k, radix = 2;
    short nbits, nbot, ntop, ndiff, n2, raddiv2;
    nbits = 0;
    i = nx;
    while (i > 1)
    {
        i = i >> 1;
        nbits++;
    }
    raddiv2 = radix >> 1;
    nbot = nbits >> raddiv2;
    nbot = nbot << raddiv2 - 1;
    ndiff = nbits & raddiv2;
    ntop = nbot + ndiff;
    n2 = 1 << ntop;
    index[0] = 0;
    for ( i = 1, j = n2/radix + 1; i < n2 - 1; i++)
    {
        index[i] = j - 1;
        for (k = n2/radix; k*(radix-1) < j; k /= radix)
            j -= k*(radix-1);
        j += k;
    }
    index[n2 - 1] = n2 - 1;
}
