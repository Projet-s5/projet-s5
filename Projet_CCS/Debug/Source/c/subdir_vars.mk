################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Source/c/Audio_driver.c \
../Source/c/C6713Helper_UdeS.c \
../Source/c/Correlation.c \
../Source/c/FFT.c \
../Source/c/Fenetre.c \
../Source/c/Filtre.c \
../Source/c/Fonction_projet.c \
../Source/c/Interface.c \
../Source/c/SPI_driver.c \
../Source/c/Tableau_Vibration.c \
../Source/c/Traitement_son.c \
../Source/c/Traitement_temperature.c \
../Source/c/Traitement_vibration.c \
../Source/c/Trame.c \
../Source/c/Twiddle.c \
../Source/c/Variables_globales.c \
../Source/c/main.c 

C_DEPS += \
./Source/c/Audio_driver.d \
./Source/c/C6713Helper_UdeS.d \
./Source/c/Correlation.d \
./Source/c/FFT.d \
./Source/c/Fenetre.d \
./Source/c/Filtre.d \
./Source/c/Fonction_projet.d \
./Source/c/Interface.d \
./Source/c/SPI_driver.d \
./Source/c/Tableau_Vibration.d \
./Source/c/Traitement_son.d \
./Source/c/Traitement_temperature.d \
./Source/c/Traitement_vibration.d \
./Source/c/Trame.d \
./Source/c/Twiddle.d \
./Source/c/Variables_globales.d \
./Source/c/main.d 

OBJS += \
./Source/c/Audio_driver.obj \
./Source/c/C6713Helper_UdeS.obj \
./Source/c/Correlation.obj \
./Source/c/FFT.obj \
./Source/c/Fenetre.obj \
./Source/c/Filtre.obj \
./Source/c/Fonction_projet.obj \
./Source/c/Interface.obj \
./Source/c/SPI_driver.obj \
./Source/c/Tableau_Vibration.obj \
./Source/c/Traitement_son.obj \
./Source/c/Traitement_temperature.obj \
./Source/c/Traitement_vibration.obj \
./Source/c/Trame.obj \
./Source/c/Twiddle.obj \
./Source/c/Variables_globales.obj \
./Source/c/main.obj 

OBJS__QUOTED += \
"Source\c\Audio_driver.obj" \
"Source\c\C6713Helper_UdeS.obj" \
"Source\c\Correlation.obj" \
"Source\c\FFT.obj" \
"Source\c\Fenetre.obj" \
"Source\c\Filtre.obj" \
"Source\c\Fonction_projet.obj" \
"Source\c\Interface.obj" \
"Source\c\SPI_driver.obj" \
"Source\c\Tableau_Vibration.obj" \
"Source\c\Traitement_son.obj" \
"Source\c\Traitement_temperature.obj" \
"Source\c\Traitement_vibration.obj" \
"Source\c\Trame.obj" \
"Source\c\Twiddle.obj" \
"Source\c\Variables_globales.obj" \
"Source\c\main.obj" 

C_DEPS__QUOTED += \
"Source\c\Audio_driver.d" \
"Source\c\C6713Helper_UdeS.d" \
"Source\c\Correlation.d" \
"Source\c\FFT.d" \
"Source\c\Fenetre.d" \
"Source\c\Filtre.d" \
"Source\c\Fonction_projet.d" \
"Source\c\Interface.d" \
"Source\c\SPI_driver.d" \
"Source\c\Tableau_Vibration.d" \
"Source\c\Traitement_son.d" \
"Source\c\Traitement_temperature.d" \
"Source\c\Traitement_vibration.d" \
"Source\c\Trame.d" \
"Source\c\Twiddle.d" \
"Source\c\Variables_globales.d" \
"Source\c\main.d" 

C_SRCS__QUOTED += \
"../Source/c/Audio_driver.c" \
"../Source/c/C6713Helper_UdeS.c" \
"../Source/c/Correlation.c" \
"../Source/c/FFT.c" \
"../Source/c/Fenetre.c" \
"../Source/c/Filtre.c" \
"../Source/c/Fonction_projet.c" \
"../Source/c/Interface.c" \
"../Source/c/SPI_driver.c" \
"../Source/c/Tableau_Vibration.c" \
"../Source/c/Traitement_son.c" \
"../Source/c/Traitement_temperature.c" \
"../Source/c/Traitement_vibration.c" \
"../Source/c/Trame.c" \
"../Source/c/Twiddle.c" \
"../Source/c/Variables_globales.c" \
"../Source/c/main.c" 


