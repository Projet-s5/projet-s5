/********************************************************
**  Session 5 - APP6 - Téléphonie par DSP
**  Fichier Audio_driver.h
**  Auteurs : < vos noms >
**  Date : < derniere modification >
********************************************************/


#ifndef INCLUDES_AUDIO_MODULE_
#define INCLUDES_AUDIO_MODULE_



// standard libraries 
#include <stdio.h>   // get standard I/O functions (as printf)
#include <stddef.h>  // get null and size_t definition
#include <stdbool.h> // get boolean, true and false definition



// Used modules headers
#include "C6713Helper_UdeS.h"
#include "Variables_globales.h"
#include "Filtre.h"

// Defines
#define DSK6713_AIC23_INPUT_MIC 0x0015
#define DSK6713_AIC23_INPUT_LINE 0x0011



// Function Prototypes
extern void Audio_init(void);
interrupt void c_int11(void);
void ReceptionValeurSon(short son);
void ReceptionValeurVibration(short vibration);
void DemanderLectureTemperature();
void CopierBufferTemperaure();
void CopierBufferSon();
void CopierBufferVibration();
extern unsigned short messageAEnvoyer;



// Global variables




#endif // end of #ifndef INCLUDES_AUDIO_MODULE_

// end of Audio_driver.h
