/*
 * FFT.h
 *
 *  Created on: Apr 1, 2019
 *      Author: charles-frederick
 */

#ifndef INCLUDES_FFT_H_
#define INCLUDES_FFT_H_

#include "C:\ti\ccsv7\ccs_base\C6700\dsplib\include\DSPF_sp_cfftr2_dit.h"
#include "C:\ti\ccsv7\ccs_base\C6700\dsplib\include\DSPF_sp_bitrev_cplx.h"
#include "filtreFIR.h"
#include "Twiddle.h"
#include "Variables_globales.h"
#include <math.h>


#define ERROR_FFT           666
#define SIGNAL_DETECTED     21
#define NO_SIGNAL_DETECTED  22
#define FREQ_CENTER         8000 //Hz
#define FREQ_MARGIN         500 // Hz
#define length256           256
#define length512           512
#define length1024          1024

extern float fftRes[2*length256];


unsigned short FFTProcess(int* sig, unsigned lengthSig); //returns 1 if error.
int FFTAverage(float* sig, unsigned lengthSig);
int FFTAverageLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTAverageComplex(float* sig, unsigned lengthSig);
int FFTAverageComplexLocal(int* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTMax(int* sig, unsigned lengthSig);
int FFTMaxLocal(float* sig, unsigned lengthSig, unsigned startID, unsigned endID);

int FFTMaxComplex(int* sig, unsigned lengthSig);
int FFTMaxComplexLocal(float* sig, unsigned lengthSig, unsigned startID, unsigned endID);

void bitrev_index(short *index, int nx);

#endif /* INCLUDES_FFT_H_ */
