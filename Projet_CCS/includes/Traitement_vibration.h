/*
 * Traitement_vibration.h
 *
 *  Created on: 31 mars 2019
 *      Author: pels2511
 */

#ifndef INCLUDES_TRAITEMENT_VIBRATION_H_
#define INCLUDES_TRAITEMENT_VIBRATION_H_

#include <stdbool.h>
#include "Variables_globales.h"
#include "Correlation.h"


static int ERROR_CORR = 667;
static int INSIDE_CORR_THRESHOLD = 23;
static int OUTSIDE_CORR_THRESHOLD = 24;

static int CORR_THRESHOLD_MAX = 50000000; //la valeur que une erreur devrait etre detected
static int CORR_THRESHOLD_MIN = 20000000;

extern bool VibrationCalib;
extern bool CalibStarted;

unsigned short DetecterNiveauVibration(int* sig1,  unsigned length1);

bool VibrationEstCalibre();
bool StartVibrationCalibration();
bool CalibrerVibration(int* trameCalib);
int VibrationCorrProcess(int* sig1,  unsigned length1, int* sig2, unsigned length2, int* corrRes);

#endif /* INCLUDES_TRAITEMENT_VIBRATION_H_ */
