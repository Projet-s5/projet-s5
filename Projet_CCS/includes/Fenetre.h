/*
 * Fenetre.h
 *
 *  Created on: Apr 4, 2019
 *      Author: charles-frederick
 */

#ifndef INCLUDES_FENETRE_H_
#define INCLUDES_FENETRE_H_

void fenetreHamming(int *Trame, short N);

#endif /* INCLUDES_FENETRE_H_ */
