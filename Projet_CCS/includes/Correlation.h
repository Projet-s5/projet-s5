/*
 * Correlation.h
 *
 *  Created on: Mar 17, 2019
 *      Author: charles-frederick
 */

#ifndef INCLUDES_CORRELATION_H_
#define INCLUDES_CORRELATION_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dsk6713.h>

#include "Fonction_projet.h"
#include "DSK6713_AIC23.h" //codec support
#include "TableauSignaux.h"


void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes);

void ASMCorr(int* sig1,unsigned length1,int* sig2,unsigned length2,int* corrRes);//unsigned sizeRes);
void Test();
void ThresholdIdentification();

int CorrMax(int* sig, unsigned lengthSig);
unsigned long CorrAvg(int* sig, unsigned lengthSig);
unsigned long CorrSum(int* sig, unsigned lengthSig);

#endif /* INCLUDES_CORRELATION_H_ */
