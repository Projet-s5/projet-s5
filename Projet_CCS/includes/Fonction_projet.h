/* Fonction.h */



#ifndef INCLUDES_FONCTION_PROJET_H_
#define INCLUDES_FONCTION_PROJET_H_



//constante a redefinir... pt utile plus tard ...
//#define DSK6713_USER_REG 0x90080000         //Addresse du CPLD register

//constante
#include <stdio.h>
#include <stdlib.h>
#include <dsk6713.h>
#include "SPI_driver.h"

#include"Variables_globales.h"
#include "DSK6713_AIC23.h" //codec support

#include"csl.h"
#include"csl_gpio.h"
#include"dsk6713_led.h"
#include"dsk6713_dip.h"

#define MASK_RELAIS 0x1;
#define FAUX 0
#define NIVEAU_CRITIQUE 3
#define NIVEAU_NORMAL 1
#define NIVEAU_SUSPECT 2
#define RESET_TEMOIN 3
//#define TAILLE_SON 0 // a determiner
//#define TAILLE_TEMPERATURE 0 // a determiner
//#define TAILLE_VIBRATION 2500 // a determiner
#define VRAI 1

//Adresse des registres importants
//#define DSK6713_MISC 0x90080006         //Addresse du CPLD pour le MISC

#define McBSP0_R 0x018C0000 //receive
#define McBSP1_R 0x01900000 //receive

#define McBSP0_T 0x018C0004
#define McBSP1_T 0x01900004

//variable globale
//extern int moteurMarche;



//Fonction
void enableInterrupts(void);
void configAndStartTimer1(void);
void EteindreTemoinLumineux();
void LED0(void);
void LED1(void);
void LED2(void);
void LED3(void);
void traduireMessage(int valeurMessage);
void DemarrerMoteur();
void ArreterMoteur();
void patcher_tableau();
unsigned int traduireEnvoi(unsigned int messageCourant, unsigned short niveauMesure, unsigned short decalage);

//correlation
/*
void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);
void ASMCorrCall(short* sig1, unsigned length1, short* sig2, unsigned length2, short* corrRes, unsigned* sizeRes);
void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes);
void ASMCorrCall(short* sig1, unsigned length1, short* sig2, unsigned length2, short* corrRes, unsigned* sizeRes);
*/


#endif /* INCLUDES_FONCTION_PROJET_H_ */

