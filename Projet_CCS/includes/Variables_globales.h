/*
 * Variables_global.h
 *
 *  Created on: 31 mars 2019
 *      Author: pels2511
 */

#ifndef INCLUDES_VARIABLES_GLOBALES_H_
#define INCLUDES_VARIABLES_GLOBALES_H_


//constantes commandes
#define ARRETER 1
#define DEMARRER 2
#define REFERENCE 6

//constantes messages envoy�s
#define NIVEAU_NORMAL 1
#define NIVEAU_SUSPECT 2
#define NIVEAU_CRITIQUE 3
#define MESURE_TEMPERATURE 4
#define MESURE_SON 0
#define MESURE_VIBRATION 2

//constantes seuils
#define SEUIL_SUSPECT 0.75
#define SEUIL_CRITIQUE 0.5

//constantes prise de mesure
#define FREQ_SON 24000
#define FREQ_VIBRATION 480
#define FREQ_TEMPERATURE 1
#define TEMPS_VIBRATION (FREQ_SON/FREQ_VIBRATION)
#define TEMPS_TEMPERATURE (FREQ_VIBRATION/FREQ_TEMPERATURE)
#define TAILLE_SON 256 // a determiner
#define TAILLE_VIBRATION 128 // a determiner
#define TAILLE_TEMPERATURE 1

//autres constantes
#define FAUX 0
#define VRAI 1

//variable globale

// variables pour le son
extern int bufferSon[];
extern int tableauSon[];
extern int tableauSonReference[];
extern unsigned short indexSon;
extern unsigned short sonComplete;
extern unsigned short sonReferenceOK;


// variables pour la vibration
extern int bufferVibration[];
extern int tableauVibration[];
extern int tableauVibrationReference[];
extern unsigned short indexVibration;
extern unsigned short vibrationComplete;
extern unsigned short vibrationReferenceOK;
extern unsigned short timerVibration;


// variables pour la temperature
extern int bufferTemperature;
extern int temperature;
extern int temperatureReference;
extern unsigned short temperatureComplete;
extern unsigned short temperatureReferenceOK;
extern unsigned short timerTemperature;


// autres variables
extern unsigned short etablirValeursReferences;
extern unsigned short commencerProgramme;
extern unsigned int message;
extern unsigned int messageRecu;
extern unsigned short moteurMarche;





#endif /* INCLUDES_VARIABLES_GLOBALES_H_ */
