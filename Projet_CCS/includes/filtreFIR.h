/*
 * filtreFIR.h
 *
 *  Created on: 2016-10-23
 *      Author: chap1202
 *
 */

#ifndef INCLUDES_FILTREFIR_H_
#define INCLUDES_FILTREFIR_H_

extern short * filtreFIR(short *, short, const short *, short *y);

#endif /* INCLUDES_FILTREFIR_H_ */
