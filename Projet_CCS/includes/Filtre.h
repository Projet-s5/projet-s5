/*
 * Filtre.h
 *
 *  Created on: Apr 12, 2019
 *      Author: charles-frederick
 */

#ifndef INCLUDES_FILTRE_H_
#define INCLUDES_FILTRE_H_

//Ne pas call, est la pour rendre accessible la fn en asm
extern short * filtreFIR(short *, short, const short *, short *y);

//Appeller ces fonctions pour faire le filtrage.
short* filtreFIR63(short*,short, const short*, short*); //Filtrage FIR avec 63 coefficients



#endif /* INCLUDES_FILTRE_H_ */
