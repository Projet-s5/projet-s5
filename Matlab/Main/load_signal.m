function [ x,y ] = load_signal( filename )

tableau = csvread(filename,0,3);
x = tableau(1:end,1);
y = tableau(1:end,2);

end

