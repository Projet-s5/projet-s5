function [CorrVector,Nres] = ManCorr(Signal1In,Signal2In)
    % This function does the correlation between two input signals, which
    % does not have to be the same size. The inputs are the two signals
    % desired, and the output is [CorrVector,Nres].

    %Define which signal is the biggest, and pad the other if needed
    N = 0;
    N1 = length(Signal1In)
    N2 = length(Signal2In)
    if((N1-N2) > 0)
        Signal2In = [Signal2In zeros(1,abs(N1-N2))];
        N = N1;
        disp('N1>N2')
    elseif((N1-N2) < 0)
        Signal1In = [Signal1In zeros(1,abs(N1-N2))]; 
        N = N2;
        disp('N1<N2')
    else
        N = N1;
        disp('N1=N2')
    end
    length(Signal1In)
    figure()
    plot(Signal1In)
    length(Signal2In)
    figure()
    plot(Signal2In)
    Nres = 2*N-1;
    CorrVector = zeros(1,Nres);
    for n = 1:Nres
        if (n<N)
            for i = 1:n
                CorrVector(Nres-n+1) = CorrVector(Nres-n+1)+Signal2In(i)*Signal1In(end-n+i);
            end
        else
            for i = 1:(2*N-n)
                CorrVector(Nres-n+1) = CorrVector(Nres-n+1)+Signal2In(end-(2*N-n)+i)*Signal1In(i);
            end
        end
    end
end