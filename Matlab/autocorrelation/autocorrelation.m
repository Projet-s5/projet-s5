%% Version autocorrelation avec une fonction
%Par Charles-Frederick
clc
close all

Fe = 10000;
n = [1:1000];
N1 = length(n);
T = 1/Fe;
y = sin(pi/2*n*T);

C = xcorr(y,y);

[ResCorr, N] = ManCorr(y,y);

figure;
subplot(3,1,1);
stem(C);
title('xcorr');
subplot(3,1,2);
stem(ResCorr);
title('autocorrelation avec boucle for');
subplot(3,1,3);
stem(C-ResCorr);
title('Dff�rence entre xcorr et la boucle');

%% Version correlation avec une fonction

clc
close all
clear all

Fe = 10000;
n_a = [1:1000];
N1 = length(n_a);
T = 1/Fe;

y1 = sin(pi/2*n_a*T);
y2 = [zeros(1,length(n_a)) y1 zeros(1,length(n_a))];
%y1 = [1 2 3 4 5 6 7 8]
%y2 = [2 3 4 5 6 7 8 9]
C = xcorr(y1,y1);

[CorrVector, N] = ManCorr(y1,y1);

figure;
subplot(3,1,1);
stem(C);
title('xcorr sin');
subplot(3,1,2);
stem(CorrVector);
title('autocorrelation avec boucle for sin');
subplot(3,1,3);
stem(C-CorrVector);
title('Dff�rence entre xcorr et la boucle sin');

%% Version correlation avec une fonction

clc
close all
clear all

Fe = 10000;
n_a = [1:1000];
N1 = length(n_a);
T = 1/Fe;

y1 = sin(pi/2*n_a*T);
y2 = [zeros(1,length(n_a)) y1 zeros(1,length(n_a))];
%y1 = [1 2 3 4 5 6 7 8]
%y2 = [2 3 4 5 6 7 8 9]
C = xcorr(y1,y2);

[CorrVector, N] = ManCorr(y1,y2);

figure;
subplot(3,1,1);
stem(C);
title('xcorr signal decal�');
subplot(3,1,2);
stem(CorrVector);
title('autocorrelation avec boucle for signal d�cal�');
subplot(3,1,3);
stem(C-CorrVector);
title('Dff�rence entre xcorr et la boucle signal d�cal�');
