clc
clear
close all

SeuilSuspect = 0.75;
SeuilCritique = 0.5;


[tempsRef,ampRef] = load_signal('Sans_Charge2.CSV');
[tempsBruit1,ampBruit1] = load_signal('Bruit1.CSV');
[tempsBruit2,ampBruit2] = load_signal('Bruit2.CSV');
[tempsBruit3,ampBruit3] = load_signal('Bruit3.CSV');

ampRef = ampRef .* 1000;
ampBruit1 = ampBruit1 .* 1000;
ampBruit2 = ampBruit2 .* 1000;
ampBruit3 = ampBruit3 .* 1000;

figure()
plot(tempsRef,ampRef)
figure()
plot(tempsBruit1,ampBruit1)
figure()
plot(tempsBruit2,ampBruit2)
figure()
plot(tempsBruit3,ampBruit3)

%%
clc
close all

CReference_man = ManCorr(ampRef,ampRef);
CBruit1_man = ManCorr(ampRef,ampBruit1);
CBruit2_man = ManCorr(ampRef,ampBruit2);
CBruit3_man = ManCorr(ampRef,ampBruit3);


%%
clc
close all

figure
stem(CReference_man)
title('Sans Charge ManCorr')

figure
stem(CBruit1_man)
title('Avec Charge1 ManCorr')

figure
stem(CBruit2_man)
title('Avec Charge2 ManCorr')

figure
stem(CBruit3_man)
title('Avec Charge3 ManCorr')




%% test signal gain
clc
close all

[mCRef_man] = max(CReference_man);
[mcB1_man] = max(CBruit1_man);
[mcB2_man] = max(CBruit2_man);
[mcB3_man] = max(CBruit3_man);

RefSuspect = SeuilSuspect*mCRef_man;
RefCritique = SeuilCritique*mCRef_man;

if(mcB1_man > RefCritique)
    if(mcB1_man > RefSuspect)
        seuil1 = 'normal';
    else
        seuil1 = 'suspect';
    end
    
else
    seuil1 = 'critique';
end

if(mcB2_man > RefCritique)
    if(mcB2_man > RefSuspect)
        seuil2 = 'normal';
    else
        seuil2 = 'suspect';
    end
    
else
    seuil12 = 'critique';
end

if(mcB3_man > RefCritique)
    if(mcB3_man > RefSuspect)
        seuil3 = 'normal';
    else
        seuil3 = 'suspect';
    end
    
else
    seuil3 = 'critique';
end

%%
clc

csvwrite('ampRef.txt',ampRef')
csvwrite('ampBruit1.txt',ampBruit1')
csvwrite('ampBruit2.txt',ampBruit2')
csvwrite('ampBruit3.txt',ampBruit3')
    
    
    
%%
clc
close all


sig1 = [1 2 3 2 1];
sig2 = [9 8 7 6 5];

csig = ManCorr(sig1,sig2)

figure
stem(csig)






    

