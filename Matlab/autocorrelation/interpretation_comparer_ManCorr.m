clc
clear
close all


[tempsRef,ampRef] = load_signal('Sans_Charge2.CSV');
[tempsBruit1,ampBruit1] = load_signal('Bruit1.CSV');
[tempsBruit2,ampBruit2] = load_signal('Bruit2.CSV');
[tempsBruit3,ampBruit3] = load_signal('Bruit3.CSV');

figure()
plot(tempsRef,ampRef)
figure()
plot(tempsBruit1,ampBruit1)
figure()
plot(tempsBruit2,ampBruit2)
figure()
plot(tempsBruit3,ampBruit3)

%%
clc
close all

CReference_x = xcorr(ampRef,ampRef);
CBruit1_x = xcorr(ampRef,ampBruit1);
CBruit2_x = xcorr(ampRef,ampBruit2);
CBruit3_x = xcorr(ampRef,ampBruit3);

CReference_man = ManCorr(ampRef,ampRef);
CBruit1_man = xcorr(ampRef,ampBruit1);
CBruit2_man = xcorr(ampRef,ampBruit2);
CBruit3_man = xcorr(ampRef,ampBruit3);


%%
clc
close all

figure
subplot(3,1,1)
stem(CReference_x)
title('Sans Charge xcorr')
subplot(3,1,2)
stem(CReference_man)
title('Sans Charge ManCorr')
subplot(3,1,3)
stem(CReference_x'-CReference_man)
title('Difference entre les corrélations')

figure
subplot(3,1,1)
stem(CBruit1_x)
title('Avec Charge1 xcorr')
subplot(3,1,2)
stem(CBruit1_man)
title('Avec Charge1 ManCorr')
subplot(3,1,3)
stem(CBruit1_x-CBruit1_man)
title('Difference entre les corrélations')

figure
subplot(3,1,1)
stem(CBruit2_x)
title('Avec Charge2 xcorr')
subplot(3,1,2)
stem(CBruit2_man)
title('Avec Charge2 ManCorr')
subplot(3,1,3)
stem(CBruit2_x-CBruit2_man)
title('Difference entre les corrélations')

figure
subplot(3,1,1)
stem(CBruit3_x)
title('Avec Charge3 xcorr')
subplot(3,1,2)
stem(CBruit3_man)
title('Avec Charge3 ManCorr')
subplot(3,1,3)
stem(CBruit3_x-CBruit3_man)
title('Difference entre les corrélations')




%% test signal gain
[mCRef_x] = max(CReference_x);
[mcB1_x] = max(CBruit1_x);
[mcB2_x] = max(CBruit2_x);
[mcB3_x] = max(CBruit3_x);

[mCRef_man] = max(CReference_man);
[mcB1_man] = max(CBruit1_man);
[mcB2_man] = max(CBruit2_man);
[mcB3_man] = max(CBruit3_man);


if(mcB1_x < mCRef_x)
    detectionGain1_x = 1;
else
    detectionGain1_x = 0;
end

if(CBruit2_x < mCRef_x)
    detectionGain2_x = 1;
else
    detectionGain2_x = 0;
end

if(CBruit2_x < mCRef_x)
    detectionGain3_x = 1;
else
    detectionGain3_x = 0;
end





if(mcB1_man < mCRef_man)
    detectionGain1_man = 1;
else
    detectionGain1_man = 0;
end

if(CBruit2_man < mCRef_man)
    detectionGain2_man = 1;
else
    detectionGain2_man = 0;
end

if(CBruit2_man < mCRef_man)
    detectionGain3_man = 1;
else
    detectionGain3_man = 0;
end


