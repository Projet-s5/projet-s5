/* Fonction.h */

//constante a redefinir... pt utile plus tard ...
#define DSK6713_USER_REG 0x90080000         //Addresse du CPLD register
#define TIMER_1_CTL 0x01980000 //si de besoin ...
#define TIMER_1_PRD 0x01980004
#define GPEN_REG_GPIO 0x01B00000
#define GPDIR_REG_GPIO 0x01B00004
#define GPPOL_REG_GPIO 0x01B00024

#define TIMER_1_CTL 0x01980000
#define TIMER_1_PRD 0x01980004

#define DEL0_MASK_OR 0x1
#define DEL0_MASK_AND 0x1

//constante

#define ARRETER 1
#define DEMARRER 2
#define FAUX 0
#define NIVEAU_CRITIQUE 3
#define NIVEAU_NORMAL 1
#define NIVEAU_SUSPECT 2
#define RESET_TEMOIN 3
#define TAILLE_SON 2500 // a determiner
#define TAILLE_TEMPERATURE 0 // a determiner
#define TAILLE_VIBRATION 0 // a determiner
#define VRAI 1

//Adresse des registres importants
#define DSK6713_MISC 0x90080006         //Addresse du CPLD pour le MISC

#define McBSP0_R 0x018C0000 //receive
#define McBSP1_R 0x01900000 //receive

#define McBSP0_T 0x018C0004
#define McBSP1_T 0x01900004

//variable globale
extern int moteurMarche;
extern int true;
extern int false;

//Fonction
void enableInterrupts(void);
void configAndStartTimer1(void);
void EteindreTemoinLumineux();
void LED0(void);
void LED1(void);
void LED2(void);
void LED3(void);
void traduireMessage(int valeurMessage);
void DemarrerMoteur();
void ArreterMoteur();

