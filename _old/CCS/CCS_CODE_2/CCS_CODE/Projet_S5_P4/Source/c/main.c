/*
 * main.c
 */

#include <stdio.h>
#include <dsk6713.h>
#include "DSK6713_AIC23.h" //codec support
#include "Fonction_projet.h" //codec support
#include <csl_mcbsphal.h>
#include <csl_mcbsphal.h>

float signal[TAILLE_SON];
float valeurConversion[TAILLE_SON];
int messageRecu = 0;
int valeurMessage = 0;
int EtablirValeurReference = 0;
int sonComplete;


int main(void) {

    DSK6713_init(); // Initialize the board support library

    //Parametres pour DEMO
    LED0(); // En attente (demo)
    messageRecu = true;
    valeurMessage = DEMARRER;
    sonComplete = true;


    while(1)
    {
        if (messageRecu == true)
        {
           traduireMessage(valeurMessage);
           messageRecu = false;
        }

        if (moteurMarche == true && DSK6713_rget(DSK6713_USER_REG) & 0x10) //verifie que la switch0 est activer (peut importe letat des leds ou des autres switch)
        {
            EtablirValeurReference = true;
        }

        if(messageRecu == false && moteurMarche == 1 && EtablirValeurReference == true) //redefenir le true de la valeur ref pour projet mais ok pour demo
        {
            if(sonComplete == true)
            {
                //fonction de correlation a implementer
                LED1(); //detecter mais pas bon signal
                LED2(); //detecter et bon signal
            }
        }
    }






   // DSK6713_init();
   // DSK6713_rset(DSK6713_USER_REG, 0xf); //allume 4 leds (ecrit une valeur a user_reg)
   // DSK6713_rget(DSK6713_USER_REG); //read la valeur de user_reg du cpld (leds et switch)


    /*
    //AIC function
    DSK6713_AIC23_CodecHandle hCodec; // Open codec with default settings
    hCodec = DSK6713_AIC23_openCodec(0, &config);
    DSK6713_AIC23_config(hCodec, &config); // Configure codec with defaults except mute bit set (bit 3 of reg 5)
    //DSK6713_AIC23_openCodec();
     *
     */


	return 0;
}
