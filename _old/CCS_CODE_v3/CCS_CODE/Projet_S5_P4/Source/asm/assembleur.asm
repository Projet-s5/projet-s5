	.def _enableInterrupts

	.data

mask_GIE .set 0x01
mask_NMIE .set 0x8812

_enableInterrupts
	.asmfunc



	B B3
	NOP 5

	.endasmfunc
