/*
 * Interface.h
 *
 *  Created on: Mar 17, 2019
 *      Author: charles-frederick
 */

#ifndef SOURCE_C_INTERFACE_H_
#define SOURCE_C_INTERFACE_H_

#include "stdio.h"

void outputCorrelation(int* sig1, unsigned length1,unsigned outputFileNum);//, int* sig2, unsigned length2, int* sig3, unsigned length3, int* sig4, unsigned length4);
void outputTableToFile(FILE* hFile, unsigned tableLength, int* table);
//FILE openFile()

static char* filePath1 = "C:\\Users\\charles-frederick\\Desktop\\Repos\\S5H19\\S5H19.git\\Projet\\projet-s5\\output\\output1.csv";
static char* filePath2 = "C:\\Users\\charles-frederick\\Desktop\\Repos\\S5H19\\S5H19.git\\Projet\\projet-s5\\output\\output2.csv";
static char* filePath3 = "C:\\Users\\charles-frederick\\Desktop\\Repos\\S5H19\\S5H19.git\\Projet\\projet-s5\\output\\output3.csv";
static char* filePath4 = "C:\\Users\\charles-frederick\\Desktop\\Repos\\S5H19\\S5H19.git\\Projet\\projet-s5\\output\\output4.csv";

#endif /* SOURCE_C_INTERFACE_H_ */
