/*
 * Tableau_Vibration.h
 *
 *  Created on: 27 f�vr. 2019
 *      Author: melo2301
 */

#ifndef SOURCE_C_INTERPRETATION_H_
#define SOURCE_C_INTERPRETATION_H_


#include"Tableau_Vibration.h"
#include"Correlation.h"
#include"Fonction_projet.h"

int Maximum(int *tableauTest, int longeur);

int Interpretation(int numTest);


#endif /* SOURCE_C_INTERPRETATION_H_ */
