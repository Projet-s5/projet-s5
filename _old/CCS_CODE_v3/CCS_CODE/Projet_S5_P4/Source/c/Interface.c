/*
 * Interface.c
 *
 *  Created on: Mar 17, 2019
 *      Author: charles-frederick
 */

#include "stdio.h"
#include "stdlib.h"
#include "Interface.h"

void outputCorrelation(int* sig1, unsigned length1,unsigned outputFileNum)//, int* sig2, unsigned length2, int* sig3, unsigned length3, int* sig4, unsigned length4)
{
    //Ouverture des 4 fichiers de output
    if(outputFileNum == 1)
    {
        FILE* hFile1 = fopen(filePath1,"w+");
        outputTableToFile(hFile1, length1, sig1);
        fclose(hFile1);
    }
    else if(outputFileNum == 2)
    {
        FILE* hFile2 = fopen(filePath2,"w+");
        outputTableToFile(hFile2, length1, sig1);
        fclose(hFile2);
    }
    else if(outputFileNum == 3)
    {
        FILE* hFile3 = fopen(filePath3,"w+");
        outputTableToFile(hFile3, length1, sig1);
        fclose(hFile3);
    }
    else if(outputFileNum == 4)
    {
        FILE* hFile4 = fopen(filePath4,"w+");
        outputTableToFile(hFile4, length1, sig1);
        fclose(hFile4);
    }
}

void outputTableToFile(FILE* hFile, unsigned tableLength, int* table)
{
    //fprintf(hFile, "Signal1:\n");
    unsigned i = 0;
    for(; i < tableLength; i++)
    {
        fprintf(hFile, "%d,",table[i]);
    }
}
