/*
 * Correlation.c
 */

//Headers


#include"Correlation.h"




void CCorrelation(unsigned* sig1, unsigned length1, unsigned* sig2, unsigned length2, unsigned* corrRes, unsigned* sizeRes)
{
    // Pad the signals if the length is not equal
    if(length1 != length2)
    {
        if(length1 > length2)
        {
            //Pad the sig2
            length2 = length1;
        }
        else
        {
            //Pad the sig1
            length1 = length2;
        }
    }

    //Instanciate the result array
    *sizeRes = length1+length2-1;
    corrRes = calloc(*sizeRes, sizeof(unsigned int));

    //Acquire the first half of data
    unsigned int i;
    for( i = 0; i < length1 ; i++)
    {
        unsigned int j;
        for(j = 0; j < i; j++)
        {
            corrRes[length1-j+1] += sig1[length1-i+j]*sig2[length2-i];
        }
    }

    //Acquire the second half of data
    for(i = 0; i < length1; i++)
    {
        unsigned int j;
        for(j = 1; j < i; j++)
        {
            corrRes[length1+j] += sig1[length1+i-j]*sig2[length1+i];
        }
    }
    //return ResCorr;
}

void ASMCorrCall(int* sig1, unsigned length1, int* sig2, unsigned length2, int* corrRes, unsigned* sizeRes)
{
    // Pad the signals if the length is not equal
    if(length1 != length2)
    {
        if(length1 > length2)
        {
            //Pad the sig2
            length2 = length1;
        }
        else
        {
            //Pad the sig1
            length1 = length2;
        }
    }

    *sizeRes = length1+length2-1;
    //int test[9] = {0,0,0,0,0,0,0,0,0};
    ASMCorr(sig1,200,sig2,200,corrRes,sizeRes);//length1,length2
}











